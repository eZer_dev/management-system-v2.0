<div class="modal fade" id="issue_modal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-dark-400">

                <input type="hidden" id="issue_modal_mr_id" name="formkey">

                <h5 class="modal-title header_new_text text-white"> ISSUE ITEM ON REQUEST <span id="issue_modal_code_view" class="text-info">#MR/A001/001</span> </h5>

                <div class="d-flex">
                    <div class="px-1 ">
                        <a id="issue_modal_reset" class="btn btn-sm btn-default btnround"><i class="far fa-trash-alt"></i></a>
                    </div>

                    <div id="issue_modal_print_div" class="px-1">
                        <a id="issue_modal_print" class="btn btn-sm btn-default btnround"><i class="fa fa-print"></i></a>
                    </div>

                    <div class="px-1 ">
                        <a id="issue_modal_close" class="btn btn-sm btn-yellow btnround">
                            <i class="far fa-window-minimize"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">

                        <div class="row">
                            <div class="col-xl-12 mb-3">
                                <div class="card mb-3 h-100 border-1 shadow-sm">

                                    <div class="card-header bg-dark-400 border-0">

                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2 text-white">Primary Details</h6>
                                            </div>
                                            <a id="grnpd_refresh" class="text-muted mt-2" data-placement="top" title="Refresh All Feilds">
                                                <i class="fa fa-redo text-white"></i>
                                            </a>
                                        </div>

                                    </div>

                                    <div class="card-body">

                                        @include('alerts.formalert')

                                        <div class="row">

                                            <div class="col-xl-4">
                                                <div class="form-group mb-3">
                                                    <label class="form-label">
                                                        Issue Code
                                                    </label>
                                                    <input id="issue_modal_code" autofocus="autofocus" name="issue_modal_code" type="text" class="form-control" readonly />
                                                </div>
                                            </div>

                                            <div class="col-xl-4">
                                                <div class="form-group mb-3">
                                                    <label class="form-label">
                                                        Material Request Code
                                                    </label>
                                                    <input id="issue_modal_material_code" autofocus="autofocus" name="issue_modal_material_code" type="text" class="form-control" readonly />
                                                </div>
                                            </div>

                                            <div class="col-xl-4">
                                                <div class="form-group mb-3">
                                                    <label class="form-label">
                                                        Date
                                                    </label>
                                                    <input id="issue_modal_date" name="issue_modal_date" type="date" class="form-control" readonly />
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-xl-12">
                                            <div class="mb-3">
                                                <label class="form-label">Remark <span class="text-danger">*</span></label>
                                                <textarea class="form-control" id="issue_modal_remark" name="issue_modal_remark" cols="30" rows="5"></textarea>
                                                @error('issue_modal_remark')
                                                <span class="text-danger">
                                                    <small>{{ $message }}</small>
                                                </span>
                                                @enderror
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>
                            <div class="col-xl-12">

                                <div class="card mb-3 border-1 shadow-sm">

                                    <div class="card-header bg-dark-400 ">
                                        <h6 class="mt-2 text-white">Added Items for Material Request</h6>
                                    </div>

                                    <div class="card-body">

                                        <div class="row">

                                            <div class="table-responsive">
                                                <table id="issue_modal_table" class="tableMheight table table-borderless table-striped text-nowrap pt-2 w-100">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Item Code</th>
                                                            <th>Item Part Code</th>
                                                            <th>Item Name</th>
                                                            <th>Qty</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>

                                                </table>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="d-flex">

                        <div class="d-flex">
                            <div class="px-1">

                                <button id="issue_modal_submit_btn" type="submit" form="grnForm" class="btn btn-teal"> <i class='fa fa-check'></i>
                                    Save & Complete </button>
                            </div>

                            <div class="px-1">
                                <a class="btn btn-default" id="issue_modal_delete_all_btn"><i class="fa fa-trash"></i>
                                    Delete All</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
