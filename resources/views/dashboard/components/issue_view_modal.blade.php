<div class="modal fade" id="view_issue_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark-400">

                <h5 class="modal-title Available header_new_text text-white">MATERIAL REQUEST ISSUE <span id="view_issue_code" class="text-yellow" style="font-weight: 700"></span></h5>

                <div class="d-flex">
                    <div class="px-1 ">
                        <a id="view_issue_print_btn" class="btn btn-sm btn-default btnround"><i class="fa fa-print"></i></a>
                    </div>

                    <div class="px-1 ">
                        <button id="view_issue_modal_close" class="btn btn-sm btn-yellow btnround ">
                            <i class="far fa-window-minimize"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="row">

                            <div class="col-xl-12">

                                <div class="card mb-3">
                                    <div class="card-header">
                                        <h6 class="mt-2">Material Request Issue Details</h6>
                                    </div>
                                    <div class="card-body">
                                        <table>
                                            <tr>
                                                <td><b>MATERIAL REQUEST ISSUE CODE</b>&nbsp;</td>
                                                <td id="view_issue_mric"></td>
                                            </tr>
                                            <tr>
                                                <td><b>MATERIAL REQUEST CODE</b>&nbsp;</td>
                                                <td id="view_issue_mrc"></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATE</b>&nbsp;</td>
                                                <td id="view_issue_date"></td>
                                            </tr>
                                            <tr>
                                                <td><b>ISSUED BY</b>&nbsp;</td>
                                                <td id="view_issue_issued_by"></td>
                                            </tr>
                                            <tr id="view_issue_remark_tr">
                                                <td><b>REMARK</b>&nbsp;</td>
                                                <td id="view_issue_remark"></td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-xl-12 mb-3">

                                        <div class="card shadow-sm mb-3 border-1 h-100">

                                            <div class="card-header">
                                                <h6 class="mt-2">Material Request Issues</h6>
                                            </div>

                                            <div class="card-body">

                                                <div class="row">

                                                    <div class="col-xl-12">

                                                        <div class="table-responsive">
                                                            <table class="table table-striped text-nowrap pt-2 w-100">
                                                                <thead>
                                                                    <tr>
                                                                        <th class='bg-dark text-white'>#</th>
                                                                        <th class='bg-dark text-white'>Item Code</th>
                                                                        <th class='bg-dark text-white'>Part Code</th>
                                                                        <th class='bg-dark text-white'>Item Name</th>
                                                                        <th class='bg-dark text-white'>Qty</th>
                                                                        <th class='bg-dark text-white'>Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="view_issue_data"></tbody>
                                                            </table>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-xl-12 mb-3">

                                        <div class="card shadow-sm mb-3 border-1 h-100">

                                            <div class="card-header">
                                                <h6 class="mt-2">Material Request Issue Returns</h6>
                                            </div>

                                            <div class="card-body">

                                                <div class="row">

                                                    <div class="col-xl-12">

                                                        <div class="table-responsive">
                                                            <table class="table table-striped text-nowrap pt-2 w-100">
                                                                <thead>
                                                                    <tr>
                                                                        <th class='bg-dark text-white'>#</th>
                                                                        <th class='bg-dark text-white'>Item Code</th>
                                                                        <th class='bg-dark text-white'>Part Code</th>
                                                                        <th class='bg-dark text-white'>Item Name</th>
                                                                        <th class='bg-dark text-white'>Qty</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="view_issue_return_data"></tbody>
                                                            </table>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
