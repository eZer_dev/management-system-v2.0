<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JOB Report</title>

    <style>
        @page {
            size: A4;
            margin: 0;
        }

        @media print {

            html,
            body {
                width: 210mm;
                height: 297mm;
                padding-left: 10px;
                padding-right: 20px;
                padding-top: 10px;
                padding-bottom: 10px;
            }

        }

        .font {
            font-family: 'Segoe UI';
        }

        .text-center {
            text-align: center;
        }


        .row {
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            margin-top: 5px;
        }

        .col-2 {
            width: 16.66%;
        }


        .col-3 {
            width: 25%;
        }

        .col-4 {
            width: 33.33%
        }

        .col-6 {
            width: 50%;
        }

        .tborderth {
            border-top: 1px solid #212121;
            /* border: 1px solid black; */
            padding: 5px;
            margin: 0px;

        }

        .tbleft {
            padding-left: 10px;
            border-left: 1px solid #212121
        }

        .tbright {
            padding-right: 10px;
            border-right: 1px solid #212121
        }

        .tborder {
            /* border-left: 1px solid #212121; */
            /* border-right: 1px solid #212121; */
            /* border-top: 1px solid #212121; */
            border-bottom: 1px solid #212121;
            /* padding: 5px; */
            padding-top: 10px;
            padding-bottom: 10px;
            margin: 0px;

        }

        .alright {
            text-align: right
        }

        .smargin {
            padding: 5px;
        }

        .bold-100 {
            font-weight: 500;
        }

        .trcolor {
            background-color: #eeeeee;
            -webkit-print-color-adjust: exact;
        }

        .text-align-right {
            margin-left: auto;
            margin-right: 0px;
        }

        .text-center {
            text-align: center;
        }

    </style>

</head>

<body class="font">

    <div class="text-center">
        <h3>TRUST PLASTIC INDUSTRIES PRIVATE LIMITED</h3>
        <span>No. 451/6, Makola North, Makola - 11640</span>
        <h3>JOB REPORT</h3>
    </div>

    <br>
    <div style="padding: 0px">

        <div class="row">
            <div class="col-5">
                <table>
                    <tr>
                        <td><b>JOB #</b></td>
                        <td>&nbsp;</td>
                        <td>TPJ005</td>
                    </tr>
                    <tr>
                        <td><b>Job Date</b></td>
                        <td>&nbsp;</td>
                        <td>06/06/2021</td>
                    </tr>
                    <tr>
                        <td><b>Store Location</b></td>
                        <td>&nbsp;</td>
                        <td>Polgahawela Store</td>
                    </tr>
                    <tr>
                        <td><b>Vehicle & Model #</b></td>
                        <td>&nbsp;</td>
                        <td>Z100 (Zotye)</td>
                    </tr>
                </table>
            </div>

            <div class="col-5" style="margin-left: auto; margin-right: 0px;">
                <table>
                    <tr>
                        <td><b>Approval Status</b></td>
                        <td>&nbsp;</td>
                        <td>Approved</td>
                    </tr>

                    <tr>
                        <td><b>Approved By</b></td>
                        <td>&nbsp;</td>
                        <td>eZer Priyashan</td>
                    </tr>

                    <tr>
                        <td><b>Approved Date</b></td>
                        <td>&nbsp;</td>
                        <td>06/06/2021</td>
                    </tr>

                    <tr>
                        <td><b>Print Date</b></td>
                        <td>&nbsp;</td>
                        <td>11/06/2021</td>
                    </tr>

                </table>
            </div>

        </div>

        <br>
        <br>

        <div>
            <table class="table-border"
                style="border-spacing: 0; border-width: 0; padding: 0; border-width: 0; width:100%">
                <thead>
                    <tr class="trcolor">
                        <th class="tborderth tborder tbleft bold-100" style="text-align: left">#</th>
                        <th class="tborderth tborder bold-100" style="text-align: left">BIN</th>
                        <th class="tborderth tborder bold-100" style="text-align: left">Product Code</th>
                        <th class="tborderth tborder bold-100" style="text-align: right">Unit L/C</th>
                        <th class="tborderth tborder bold-100" style="text-align: right">Qty</th>
                        <th class="tborderth tborder bold-100" style="text-align: right">VAT %</th>
                        <th class="tborderth tborder bold-100" style="text-align: right">Sub Total</th>
                        <th class="tborderth tborder bold-100" style="text-align: right">Tot O/Exp</th>
                        <th class="tborderth tborder bold-100 tbright" style="text-align: right">Net Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tborder tbleft">1</td>
                        <td class="tborder">06-A-001</td>
                        <td class="tborder">ZO/Z1/001/HE/004</td>
                        <td class="tborder alright">15,478.00</td>
                        <td class="tborder alright">3</td>
                        <td class="tborder alright">8.5</td>
                        <td class="tborder alright">47,594.85</td>
                        <td class="tborder alright">10,000.00</td>
                        <td class="tborder alright tbright">57,594.85</td>
                    </tr>

                    <tr>
                        <td class="tborder tbleft">2</td>
                        <td class="tborder">06-A-002</td>
                        <td class="tborder">ZO/Z1/001/HE/005</td>
                        <td class="tborder alright">15,478.00</td>
                        <td class="tborder alright">3</td>
                        <td class="tborder alright">8.5</td>
                        <td class="tborder alright">47,594.85</td>
                        <td class="tborder alright">10,000.00</td>
                        <td class="tborder alright tbright">57,594.85</td>
                    </tr>

                </tbody>
            </table>

        </div>

        <div>

            <div style="text-align: center">
                <h4>EXPENSES LIST</h4>
            </div>

            <table class="table-border"
                style="border-spacing: 0; border-width: 0; padding: 0; border-width: 0; width:100%">
                <thead>
                    <tr class="trcolor">
                        <th class="tborderth tborder tbleft bold-100" style="text-align: left">#</th>
                        <th class="tborderth tborder bold-100" style="text-align: left">Product Code</th>
                        <th class="tborderth tborder bold-100" style="text-align: left">Expense Name</th>
                        <th class="tborderth tborder bold-100" style="text-align: left">Expense Reference</th>
                        <th class="tborderth tborder tbright bold-100" style="text-align: right">Expense Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>

                    <tr>
                        <td class="tborder tbleft">1</td>
                        <td class="tborder">ZO/Z1/001/HE/004</td>
                        <td class="tborder">Lorem ipsum</td>
                        <td class="tborder">06-A-001</td>
                        <td class="tborder alright tbright">15,478.00</td>
                    </tr>
                    <tr>
                        <td class="tborder tbleft tbright" colspan="5">
                            <b>Expense Remark :</b> In publishing and graphic design, Lorem ipsum is a placeholder text
                            commonly
                            used to demonstrate the visual form of a document or a typeface without relying on
                            meaningful content.
                        </td>
                    </tr>


                    </tr>

                    <tr>

                    <tr>
                        <td class="tborder tbleft">2</td>
                        <td class="tborder">ZO/Z1/001/HE/004</td>
                        <td class="tborder">Lorem ipsum</td>
                        <td class="tborder">06-A-001</td>
                        <td class="tborder alright tbright">15,478.00</td>
                    </tr>
                    <tr>
                        <td class="tborder tbleft tbright" colspan="5">
                            <b>Expense Remark :</b> In publishing and graphic design, Lorem ipsum is a placeholder text
                            commonly
                            used to demonstrate the visual form of a document or a typeface without relying on
                            meaningful content.
                        </td>
                    </tr>


                    </tr>

                </tbody>
            </table>

        </div>

    </div>
    <br>

</body>

</html>
