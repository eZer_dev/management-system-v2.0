<div class="modal fade" id="mr_view_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark-400">

                <h5 class="modal-title Available header_new_text text-white">MATERIAL REQUEST <span id="mr_job_code"
                        class="text-yellow" style="font-weight: 700">#MR/370621/001</span></h5>

                <div class="d-flex">
                    <div class="px-1 ">
                        <button class="btn btn-sm btn-default btnround"><i class="far fa-trash-alt"></i></button>
                    </div>

                    <div class="px-1 ">
                        <button class="btn btn-sm btn-default btnround"><i class="fa fa-print"></i></button>
                    </div>

                    <div class="px-1 ">
                        <button id="myi_mr_modal_view" class="btn btn-sm btn-yellow btnround">
                            <i class="far fa-window-minimize"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-xl-12">

                        <div class="row">

                            <div class="col-xl-12">

                                <div class="card mb-3">
                                    <div class="card-header">
                                        <h6 class="mt-2">Material Request Details</h6>
                                    </div>
                                    <div class="card-body">
                                        <table>
                                            <tr>
                                                <td><b>MATERIAL CODE</b>&nbsp;</td>
                                                <td>MR/370621/001</td>
                                            </tr>
                                            <tr>
                                                <td><b>JOB CODE</b>&nbsp;</td>
                                                <td>TCJ001</td>
                                            </tr>
                                            <tr>
                                                <td><b>DATE</b>&nbsp;</td>
                                                <td>16/06/2021</td>
                                            </tr>
                                            <tr>
                                                <td><b>LOCATION</b>&nbsp;</td>
                                                <td>Polgahawela Store</td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-xl-12 mb-3">

                                        <div class="card shadow-sm mb-3 border-1 h-100">

                                            <div class="card-header">
                                                <h6 class="mt-2">Add Material Request</h6>
                                            </div>

                                            <div class="card-body">

                                                <div class="row">

                                                    <div class="col-xl-12">

                                                        <div class="table-responsive">
                                                            <table id="mr_session_added_list"
                                                                class="table table-borderless table-striped text-nowrap pt-2 w-100">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>P/Code</th>
                                                                        <th>Part Code</th>
                                                                        <th>Qty</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>


                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="d-flex">

                            <div class="d-flex">

                                <div class="px-1">
                                    <button class="btn btn-yellow"> <i class='fa fa-check'></i>
                                        Approve </button>
                                </div>

                                <div class="px-1">
                                    <button class="btn btn-danger"> <i class='fa fa-close'></i>
                                        Refuse </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
