@if (isset($invoices))

@foreach ($invoices as $key=> $invoice)
<tr>
    <td class="py-1 align-middle">{{ $key+1}}</td>
    <td class="py-1 align-middle">{{ $invoice->invoice_code}}</td>
    <td class="py-1 align-middle"> {{ date('d-m-Y', strtotime($invoice->date)) }}</td>
    <td class="py-1 align-middle">{{ App\Models\location::find($invoice->location_id)->location_name }}</td>
    <td class="py-1 align-middle">{{ $invoice->billing_to}}</td>

    @if($invoice->po_no == '')
    <td class="py-1 align-middle">-</td>
    @else
    <td class="py-1 align-middle">{{ $invoice->po_no}}</td>
    @endif


    <td class="py-1 align-middle" style="text-align: right"> {{ number_format($invoice->subtotal, 2, '.', ',') }}</td>
    <td class="py-1 align-middle" style="text-align: right">{{ $invoice->discount}}</td>
    <td class="py-1 align-middle" style="text-align: right">{{ $invoice->vat}}</td>
    <td class="py-1 align-middle" style="text-align: right">{{ number_format($invoice->nettotal, 2, '.', ',') }}</td>

    <td>
        <div class="input-group flex-nowrap">
            <div class="">
                <button class="btn btn-secondary btn-sm" onclick="invoice_loadModalforView({{ $invoice->id }})">
                    View
                </button>
                <button class="btn btn-default btn-sm" onclick="invoice_print({{ $invoice->id }})">
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
            </div>
        </div>
    </td>


</tr>

@endforeach
@endif
