<?php
$rowval = 0;
?>

@foreach ($bin_location as $key=> $bin)

<?php

$location = App\Models\location::find($bin->location_id);
$item = App\Models\item::find($bin->item_id);

?>

<tr>
    <td class=" py-1 align-middle">{{ $rowval += 1 }}</td>
    <td class=" py-1 align-middle">{{ $location->location_name }}</td>
    <td class=" py-1 align-middle">{{ $bin->bin_location_name }}</td>
    <td class=" py-1 align-middle">{{ $item->item_code }}</td>
    <td class=" py-1 align-middle">{{ $item->item_name }}</td>
    @if($bin->status==1)
    <td class=" py-1 align-middle">
        <span
            class="badge bg-green-100 text-success px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center"><i
                class="fa fa-circle text-teal fs-9px fa-fw me-5px"></i>
            Active
        </span>
    </td>
    <td class=" py-1 align-middle">
        <div class="m-1">
            <a class="w-100 btn btn-round btn-default btn-sm text-success" onclick="binStatusChange({{ $bin->id }},2)">
                Deactivate
            </a>
        </div>
    </td>
    @else
    <td class=" py-1 align-middle">
        <span
            class="badge bg-red-100 text-danger px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center"><i
                class="fa fa-circle text-danger fs-9px fa-fw me-5px"></i>
            Inactive
        </span>
    </td>
    <td class=" py-1 align-middle">
        <div class="m-1">
            <a class=" w-100 btn btn-round btn-default btn-sm text-danger" onclick="binStatusChange({{ $bin->id }},1)">
                Activate
            </a>
        </div>
    </td>
    @endif
</tr>

@endforeach
