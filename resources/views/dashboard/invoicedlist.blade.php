@extends('dashboard.layouts.dashboard_app')

@section('content')

<div id="content" class="app-content">
    <div class="container-fluid">

        <div class="row justify-content-center">

            <div class="col-xl-12">

                <div class="row">

                    <div class="col-xl-12">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item header_new_text"><a href="/home">Dashboard</a></li>
                            <li class="breadcrumb-item active header_new_text">Invoiced Transactions</li>
                        </ul>

                        <h1 class="page-header header_new_text">
                            Invoiced Transactions
                        </h1>

                        <div class="row">

                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2">Purchase Order List</h6>
                                            </div>
                                            <a type="button" href="#" class="text-muted mt-2" data-toggle="tooltip"
                                                data-placement="bottom" title="Refresh Table">
                                                <i class="fa fa-redo"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body table-responsive">
                                        <table id="invoiced_list"
                                            class="table table-borderless table-striped text-nowrap pt-2 w-100 ">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">INV #</th>
                                                    <th scope="col">DATE</th>
                                                    <th scope="col">LOCATION</th>
                                                    <th scope="col">B/TO</th>
                                                    <th scope="col">PO #</th>
                                                    <th scope="col">SUB TOT</th>
                                                    <th scope="col">DISCOUNT</th>
                                                    <th scope="col">VAT</th>
                                                    <th scope="col">NET TOT</th>
                                                    <th scope="col">ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody id="po_list_tbody">

                                                @include('dashboard.components.invoicedlist')

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="invoice_viewModel">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-dark-400">

                <h5 class="modal-title header_new_text text-white">VIEW INVOICE</h5>

                <div class="d-flex">

                    <div class="px-1">
                        <button id="invoice_viewModelPrint" class="btn btn-sm btn-default btnround"><i
                                class="fa fa-print"></i>
                        </button>
                    </div>

                    <div class="px-1">
                        <button id="invoice_viewModelClose" class="btn btn-sm btn-yellow btnround"
                            onclick="invoice_closeModal()">
                            <i class="far fa-window-minimize"></i>
                        </button>
                    </div>

                </div>

            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-xl-6 mb-3">

                        <div class="card shadow-sm h-100">

                            <div class="card-body">

                                <div class="row">

                                    <div class="col-xl-12">

                                        <table>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">1.</td>
                                                <td class="font-weight-500" style="width:35%; vertical-align: top;">Invoice Code
                                                </td>
                                                <td id="invoice_viewCode">INV/123/WD</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">2.</td>
                                                <td class="font-weight-500" style="vertical-align: top;">Invoice Date
                                                </td>
                                                <td id="invoice_viewDate">01/01/2021</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">3.</td>
                                                <td class="font-weight-500" style="vertical-align: top;">Location</td>
                                                <td id="invoice_viewLocation">Main Store</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">4.</td>
                                                <td class="font-weight-500" style="vertical-align: top;">PO #</td>
                                                <td id="invoice_viewPo">PO/12554</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">5.</td>
                                                <td class="font-weight-500" style="vertical-align: top;">Billing To</td>
                                                <td id="invoice_viewBillingto">Micro Cars Pvt Ltd.</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">6.</td>
                                                <td class="font-weight-500" style="vertical-align: top;">Billing To
                                                    Address &nbsp;</td>
                                                <td id="invoice_viewBillingtoaddress">Micro Cars Ltd. Kandi Road, Paliyagoda.</td>
                                            </tr>
                                        </table>

                                    </div>

                                </div>


                            </div>
                        </div>

                    </div>
                    <div class="col-xl-6 mb-3">

                        <div class="card  shadow-sm h-100">

                            <div class="card-body">

                                <div class="row">

                                    <div class="col-xl-12">

                                        <table>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">7.</td>
                                                <td class="font-weight-500" style="width:30%; vertical-align: top;">Sub Total</td>
                                                <td id="invoice_viewSubtotal">LKR. 15,500.00</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">8.</td>
                                                <td class="font-weight-500" style="vertical-align: top;">Discount</td>
                                                <td id="invoice_viewDiscount">10 %</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">9.</td>
                                                <td class="font-weight-500" style="vertical-align: top;">VAT</td>
                                                <td id="invoice_viewVat">0 %</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">10.</td>
                                                <td class="font-weight-500" style="vertical-align: top;">Net Total &nbsp;</td>
                                                <td id="invoice_viewNettotal">LKR. 14,250.00</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-500" style="vertical-align: top;">11.</td>
                                                <td class="font-weight-500" style="vertical-align: top;">Remark</td>
                                                <td id="invoice_viewRemark">In publishing and graphic design, Lorem ipsum</td>
                                            </tr>

                                        </table>

                                    </div>

                                </div>


                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12 mb-3">
                        <div class="card shadow-sm">
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table id="invoice_productviewtable"
                                        class="table table-borderless table-striped text-nowrap pt-2 mb-2 w-100 tableMheight">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product Code</th>
                                                <th>Product Name</th>
                                                <th>Unit Price</th>
                                                <th>Qty</th>
                                                <th>Total</th>
                                                <th>BIN Location</th>
                                                <th>Job Code</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>


            </div>

        </div>
    </div>
</div>

@endsection
