@extends('dashboard.layouts.dashboard_app')

@section('content')

<div id="content" class="app-content">
    <div class="container-fluid">

        <div class="row justify-content-center">

            <div class="col-xl-12">

                <div class="row">
                    @include('alerts.formalert')
                    <div class="col-xl-12">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item header_new_text"><a href="/home">Dashboard</a></li>
                            <li class="breadcrumb-item active header_new_text">Invoice Management</li>
                        </ul>
                        <h1 class="page-header header_new_text">
                            Invoice Management
                        </h1>

                        <div class="">

                            <div class="px-2 pb-2">
                                <div class="d-flex">
                                    <div class="flex-grow-1">

                                    </div>
                                    <a type="button" id="resetinvoice" class="text-muted mt-2"
                                        data-placement="bottom"><i class="far fa-trash-alt"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="">

                                <div class="row">

                                    <div class="col-xl-8 mb-3">

                                        <div class="card  shadow-sm h-100">

                                            <div class="card-body">

                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="row">
                                                            <div class="col-xl-12">
                                                                <div class="form-group mb-3">
                                                                    <label for="product_name" class="form-label">
                                                                        Invoice Code <span class="text-danger">*</span>
                                                                    </label>
                                                                    <input id="invoice_code" type="text" class="form-control font-weight-600" value="{{ $invoiceCode }}" readonly />
                                                                </div>
                                                            </div>

                                                            <div class="col-xl-12">
                                                                <div class="form-group mb-3">
                                                                    <label for="product_name" class="form-label">
                                                                        Date <span class="text-danger">*</span>
                                                                    </label>
                                                                    <input id="invoice_date" type="date" class="form-control" />
                                                                </div>
                                                            </div>

                                                            <div class="col-xl-12">
                                                                <div class="form-group mb-3">
                                                                    <label class="form-label" for="location_id">
                                                                        Location
                                                                    </label>
                                                                    <select id="invoice_location_id"
                                                                        name="invoice_location_id" class="form-select">
                                                                        @foreach ($location as $key => $location)
                                                                        <option value="{{ $location->id }}">
                                                                            {{ $location->location_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-xl-12">
                                                                <div class="form-group mb-3">
                                                                    <label for="product_name" class="form-label">
                                                                        Remark
                                                                    </label>
                                                                    <textarea id="invoice_remark" class="form-control" id="remark"
                                                                        name="remark" rows="1" name="remark"></textarea>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-xl-6">
                                                        <div class="row">

                                                            <div class="col-xl-12">
                                                                <div class=" form-group mb-3">
                                                                    <label for="product_name" class="form-label">
                                                                        Purchase Order
                                                                    </label>
                                                                    <input id="invoice_po" type="text" class="form-control" />
                                                                </div>
                                                            </div>

                                                            <div class="col-xl-12">

                                                                <div class="form-group mb-3">
                                                                    <label for="product_name" class="form-label">
                                                                        Billing To <span class="text-danger">*</span>
                                                                    </label>
                                                                    <input id="invoice_bt" type="text" class="form-control" />
                                                                </div>
                                                            </div>

                                                            <div class="col-xl-12">
                                                                <div class="form-group mb-3">
                                                                    <label for="product_name" class="form-label">
                                                                        Billing Address <span
                                                                            class="text-danger">*</span>
                                                                    </label>
                                                                    <textarea id="invoice_ba" class="form-control" id="remark"
                                                                        name="remark" rows="5" name="remark"></textarea>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-xl-4 mb-3">
                                        <div class="card shadow-sm h-100">
                                            <div class="card-body">

                                                <div class="row">

                                                    <div class="col-xl-12">
                                                        <div class=" form-group mb-3">
                                                            <label for="product_name" class="form-label">
                                                                Sub Total
                                                            </label>
                                                            <input id="invoice_sub_tot" type="text" class="form-control font-weight-600" readonly />
                                                        </div>
                                                    </div>

                                                    <div class="col-xl-12">

                                                        <div class="row">

                                                            <div class="col-xl-12">
                                                                <div class="form-group mb-3">
                                                                    <label for="product_name" class="form-label">
                                                                        Discount (%)
                                                                    </label>
                                                                    <input id="invoice_dis" type="number" value="0" class="form-control" />
                                                                </div>
                                                            </div>

                                                            <div class="col-xl-12">
                                                                <div class="form-group mb-3">
                                                                    <label for="product_name" class="form-label">
                                                                        VAT (%) <span class="text-danger">*</span>
                                                                    </label>
                                                                    <input id="invoice_vat" type="number" value="0" class="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-12">
                                                                <div class="form-group mb-3">
                                                                    <label for="product_name" class="form-label">
                                                                        Net Total <span class="text-danger">*</span>
                                                                    </label>
                                                                    <input id="invoice_nettotal" type="text" class="form-control font-weight-600"
                                                                        readonly />
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-12 mb-3">

                                        <div class="card  shadow-sm">

                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="mb-3">
                                                            <label class="form-label">Search Product<span
                                                                    class="text-danger"> *</span></label>
                                                            <div class="input-group">
                                                                <span class="input-group-text"><i
                                                                        class="fa fa-search"></i></span>

                                                                <input type="text"
                                                                    placeholder="Type 'Product Code / Name'"
                                                                    class="form-control rounded-end"
                                                                    id="invoice_product_code" />
                                                            </div>
                                                            <input type="hidden" id="invoice_product_id"
                                                                name="invoice_product_code" />
                                                        </div>
                                                    </div>

                                                    <div class="col-xl-3">
                                                        <div class="form-group mb-3">
                                                            <label for="invoice_unit_price" class="form-label">
                                                                Unit Price <span class="text-danger">*</span>
                                                            </label>
                                                            <input id="invoice_unit_price" type="number"
                                                                class="form-control" />
                                                        </div>
                                                    </div>

                                                    <div class="col-xl-3">
                                                        <div class="form-group mb-3">
                                                            <label for="invoice_qty" class="form-label">
                                                                Qty <span class="text-danger">*</span>
                                                            </label>
                                                            <input id="invoice_qty" type="number"
                                                                class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-footer">
                                                <div class="row">
                                                    <div class="d-flex flex-row-reverse">

                                                        <div class="px-1">
                                                            <button id="invoice_product_clear_btn" class="btn btn-default"> <i class='fa fa-trash'></i>
                                                                Clear
                                                            </button>
                                                        </div>

                                                        <div class="px-1">
                                                            <button id="invoice_product_insert_to_session_btn"
                                                                class="btn btn-primary"> <i class='fa fa-plus'></i>
                                                                Add Product To Invoice
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-xl-12 mb-3">
                                        <div class="card shadow-sm">
                                            <div class="card-body">

                                                <div class="table-responsive">
                                                    <table id="invoice_session_table"
                                                        class="table table-borderless table-striped text-nowrap pt-2 mb-2 w-100">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Product Code</th>
                                                                <th>Unit Price</th>
                                                                <th>Qty</th>
                                                                <th>Total</th>
                                                                <th>BIN Location</th>
                                                                <th>Job Code</th>
                                                                <th>ACTION</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>

                                            <div class="card-footer">
                                                <div class="row">
                                                    <div class="d-flex flex-row-reverse">

                                                        <div class="px-1">
                                                            <button class="btn btn-default"> <i class='fa fa-trash'></i>
                                                                Clear All
                                                            </button>
                                                        </div>

                                                        <div class="px-1">
                                                            <button id="invoice_save_btn" class="btn btn-teal"> <i class='fa fa-print'></i>
                                                                Save & Print
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
