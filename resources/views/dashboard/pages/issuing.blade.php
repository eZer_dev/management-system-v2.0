@extends('dashboard.layouts.dashboard_app')

@section('content')

<div id="content" class="app-content">
    <div class="container-fluid">

        <div class="row justify-content-center">

            <div class="col-xl-12">

                <div class="row">

                    <div class="col-xl-12">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item header_new_text"><a href="/home">Dashboard</a></li>
                            <li class="breadcrumb-item active header_new_text">{{ Session::get('view', 'non') }}</li>
                        </ul>
                        <h1 class="page-header header_new_text">
                            {{ Session::get('view', 'non') }}
                        </h1>

                        <div class="row">

                            <div class="col-xl-12 mb-3">
                                <div class="card">

                                    <div class="card-header">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2">Filters</h6>
                                            </div>
                                            <a type="button" id="issuing_resetatag" class="text-muted mt-2" data-toggle="tooltip" data-placement="bottom" title="Refresh All Feilds">
                                                <i class="fa fa-redo"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="form-group mb-3">
                                                    <label class="form-label" for="issuing_filter_sdate">
                                                        Date From
                                                    </label>
                                                    <input type="date" id="issuing_filter_sdate" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="form-group mb-3">
                                                    <label class="form-label" for="issuing_filter_edate">
                                                        Date To
                                                    </label>
                                                    <input type="date" id="issuing_filter_edate" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xl-12">
                                                <div class="form-group d-flex flex-md-row-reverse pt-4 pt-md-0">

                                                    <div class="px-1">
                                                        <button id="issue_reset_btn" type="reset"
                                                            class="btn btn-round btn-default">
                                                            <i class="fa fa-filter"
                                                                aria-hidden="true"></i>
                                                            Refine
                                                            Filters
                                                        </button>
                                                    </div>

                                                    <div class="px-1">
                                                        <a id="issue_print_btn"
                                                            class="btn btn-purple">
                                                            <i class="fa fa-print"
                                                                aria-hidden="true"></i>
                                                            Print
                                                            Report
                                                        </a>
                                                    </div>

                                                    <div class="px-1">
                                                        <a id="submit_issue_filters"
                                                            class="btn btn-primary text-white">
                                                            <i class="fa fa-share-square-o"
                                                                aria-hidden="true"></i>
                                                            Submit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2">Issuing List</h6>
                                            </div>
                                            <a type="button" href="#" class="text-muted mt-2" data-toggle="tooltip" data-placement="bottom" title="Refresh Table">
                                                <i class="fa fa-redo"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body table-responsive">
                                        <table id="issuingTableView" class="table table-borderless table-striped text-nowrap pt-2 w-100 ">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Code</th>
                                                    <th scope="col">Material Request Code</th>
                                                    <th scope="col">Issued By</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('dashboard.components.issue_view_modal')

@endsection
