@extends('dashboard.layouts.dashboard_app') @section('content')

<div id="content" class="app-content">
    <div class="container-fluid">

        <div class="row justify-content-center">

            <div class="col-xl-12">

                <div class="row">

                    <div class="col-xl-12">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item header_new_text"><a href="/home">Dashboard</a></li>
                            <li class="breadcrumb-item active header_new_text">{{ Session::get('view', 'non') }}</li>
                        </ul>
                        <h1 class="page-header header_new_text">
                            {{ Session::get('view', 'non') }}
                        </h1>

                        <div class="row">

                            <div class="col-xl-12 mb-3">
                                <div class="card">

                                    <div class="card-header">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2">Filters</h6>
                                            </div>
                                            <a type="button" id="resetatag" class="text-muted mt-2" data-toggle="tooltip" data-placement="bottom" title="Refresh All Feilds">
                                                <i class="fa fa-redo"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="card-body">

                                        <form method="POST" id="submitform">

                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <div class="mb-3">
                                                        <label class="form-label">Existing Job Code</label>
                                                        <div class="input-group">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-search"></i>
                                                            </span>
                                                            <input type="text" placeholder="Search Job" class="form-control rounded-end" name="product_stock_jobs_sugg" id="product_stock_jobs_sugg" />
                                                            <input type="hidden" class="form-control rounded-end" id="product_stock_jobs" name="product_stock_jobs" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xl-4">
                                                    <div class="mb-3">
                                                        <label class="form-label">Vehicle <span class="text-danger">*</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-search"></i>
                                                            </span>
                                                            <input type="text" placeholder="Search Vehicle" class="form-control rounded-end" name="product_stock_vehicles_sugg" id="product_stock_vehicles_sugg" />
                                                            <input type="hidden" class="form-control rounded-end" id="product_stock_vehicles" name="product_stock_vehicles" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xl-4">
                                                    <div class="mb-3">
                                                        <label class="form-label">Product <span class="text-danger">*</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-search"></i>
                                                            </span>
                                                            <input type="text" placeholder="Product Code / Name" class="form-control rounded-end" name="product_stock_product_sugg" id="product_stock_product_sugg" />
                                                            <input type="hidden" class="form-control rounded-end" id="product_stock_product" name="product_stock_product" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xl-6">
                                                    <div class="form-group mb-3">
                                                        <label class="form-label" for="product_stock_location">
                                                            Location
                                                        </label>
                                                        <select class="form-control" name="product_stock_location" id="product_stock_location">
                                                            <option value="0" selected>None</option>
                                                            @foreach ($data['locations'] as $item)
                                                            <option value="{{ $item->id }}">{{ $item->location_name }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-xl-6">
                                                    <div class="form-group mb-3">
                                                        <label class="form-label" for="product_stock_bin">
                                                            Bin Location
                                                        </label>
                                                        <select class="form-control clearhtml" name="product_stock_bin" id="product_stock_bin">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-xl-6">
                                                    <div class="form-group mb-3">
                                                        <label class="form-label" for="product_stock_from">
                                                            Date From
                                                        </label>
                                                        <input type="date" id="product_stock_from" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-xl-6">
                                                    <div class="form-group mb-3">
                                                        <label class="form-label" for="product_stock_to">
                                                            Date To
                                                        </label>
                                                        <input type="date" id="product_stock_to" class="form-control">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-xl-12">

                                                <div class="row">

                                                    <div class="col-xl-12">


                                                        <div class="form-group mb-3 mt-4">

                                                            <div class="row">

                                                                <div class="col-xl-3">
                                                                    <div class="d-flex">
                                                                        <div class="pt-2" style="padding-right: 15px">
                                                                            <i class="fa fa-filter" aria-hidden="true"></i>&nbsp;<label class="form-check-label">Order&nbsp;by</label>
                                                                        </div>

                                                                        <div class="form-check px-3 pt-2">
                                                                            <!-- <i class="fa fa-book" aria-hidden="true"></i>&nbsp; -->
                                                                            <input class="form-check-input" type="radio" id="product_stock_product_wise" name="a" checked />
                                                                            <label class="form-check-label" for="product_stock_product_wise">Product</label>
                                                                        </div>

                                                                        <div class="form-check px-3 pt-2">
                                                                            <!-- <i class="fa fa-database" aria-hidden="true"></i>&nbsp; -->
                                                                            <input class="form-check-input" type="radio" id="product_stock_bin_wise" name="a" />
                                                                            <label class="form-check-label" for="product_stock_bin_wise">Bin</label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xl-9">
                                                                    <div class="form-group d-flex flex-md-row-reverse pt-4 pt-md-0">

                                                                        <div class="px-1">
                                                                            <button id="resetbtn" type="reset" class="btn btn-round btn-default">
                                                                                <i class="fa fa-filter" aria-hidden="true"></i>
                                                                                Refine
                                                                                Filters
                                                                            </button>
                                                                        </div>

                                                                        <div class="px-1">
                                                                            <a id="productstockprintbtn" class="btn btn-purple">
                                                                                <i class="fa fa-print" aria-hidden="true"></i>
                                                                                Print
                                                                                Report
                                                                            </a>
                                                                        </div>

                                                                        <div class="px-1">
                                                                            <a id="submitproductfilters" class="btn btn-primary text-white">
                                                                                <i class="fa fa-share-square-o" aria-hidden="true"></i>
                                                                                Submit
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                        </form>

                                    </div>



                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2">Stocks</h6>
                                            </div>
                                            <a type="button" href="#" id="product_stock_refresh_table" class="text-muted mt-2" data-toggle="tooltip" data-placement="bottom" title="Refresh Table">
                                                <i class="fa fa-redo"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body table-responsive">
                                        <table id="product_stock_table" class="tableMheight table table-borderless table-striped text-nowrap pt-2 w-100">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Product Code</th>
                                                    <th scope="col">Product Name</th>
                                                    <th scope="col">Bin Location</th>
                                                    <th scope="col">Qty</th>
                                                    <th scope="col">Low Stock</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
