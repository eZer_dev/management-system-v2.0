@extends('dashboard.layouts.dashboard_app')

@section('content')

<div id="content" class="app-content">
    <div class="container-fluid">

        <div class="row justify-content-center">

            <div class="col-xl-12">

                <div class="row">

                    <div class="col-xl-12">

                        <ul class="breadcrumb">
                            <li class="breadcrumb-item header_new_text"><a href="/home">Dashboard</a></li>
                            <li class="breadcrumb-item active header_new_text">
                                Items General Issues Items General Issue Requests
                            </li>
                        </ul>

                        <h1 class="page-header header_new_text">
                            Items General Issue Requests
                        </h1>

                        <hr class="mb-4" />

                        <div class="row">

                            <div class="row mb-3 d-flex justify-content-end">
                                <div class="ms-auto">
                                    <a id="itemGeneralIssueModalCreate_button" href="#itemGeneralIssueModal"
                                        data-bs-toggle="modal" class="btn btn-primary">
                                        <i class="fa fa-plus-circle me-1"></i>
                                        Issue New Item Request
                                    </a>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2">General Item Issue Request List</h6>
                                            </div>
                                            <a type="button" href="#" class="text-muted mt-2" data-toggle="tooltip"
                                                data-placement="bottom" title="Refresh Table">
                                                <i class="fa fa-redo"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body table-responsive">
                                        <table id="item_general_issue_requested_list"
                                            class="table table-borderless table-striped text-nowrap pt-2 w-100 ">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">GI Code</th>
                                                    <th scope="col">Location</th>
                                                    <th scope="col">Manual Code</th>
                                                    <th scope="col">Remark</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="itemGeneralIssueModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark-400">

                <h5 class="modal-title header_new_text text-white">REQUESTING NEW GENERAL ITEMS</h5>

                <div class="d-flex">

                    <div class="px-1 ">
                        <a id="itemGeneralIssueModal_reset" class="btn btn-sm btn-default btnround"><i
                                class="far fa-trash-alt"></i></a>
                    </div>

                    <div class="px-1 ">
                        <a id="itemGeneralIssueModal_close" class="btn btn-sm btn-yellow btnround">
                            <i class="far fa-window-minimize"></i>
                        </a>
                    </div>

                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">

                        <div class="row">

                            <div class="col-xl-12 mb-3">
                                <div class="card mb-3 h-100 border-1 shadow-sm">

                                    <div class="card-header bg-gradient-custom-teal border-0">

                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2 text-dark">Primary Details</h6>
                                            </div>
                                            <a id="grnpd_refresh" class="text-muted mt-2" data-placement="top"
                                                title="Refresh All Feilds">
                                                <i class="fa fa-redo text-dark"></i>
                                            </a>
                                        </div>

                                    </div>

                                    <div class="card-body">

                                        <div class="row">

                                            <div class="col-xl-6">
                                                <div class="form-group mb-3">
                                                    <label class="form-label" for="location_id">
                                                        Location
                                                        <span class="text-danger">*</span></label>
                                                    </label>
                                                    <select id="item_general_issue_location" name="location_id_view"
                                                        class="form-select">
                                                        @foreach ($location as $key => $location)
                                                        <option id="item_general_issue_location_option"
                                                            value="{{ $location->id }}">
                                                            {{ $location->location_name }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="form-group mb-3">
                                                    <label class="form-label">
                                                        Manual Code
                                                        <span class="text-danger">*</span></label>
                                                    </label>
                                                    <input id="item_general_issue_manual_code" type="text" class="form-control" />
                                                </div>
                                            </div>

                                            <div class="col-xl-12">

                                                <div class="mb-3">
                                                    <label class="form-label">Remark
                                                    </label>
                                                    <textarea class="form-control" id="item_general_issue_remark" rows="3">
                                                    </textarea>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="col-xl-12 mb-3">
                                <div class="card mb-3 h-100 border-1 shadow-sm">

                                    <div class="card-header bg-gradient-custom-teal border-0">

                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2 text-dark">Issuing Items</h6>
                                            </div>
                                            <a id="grnpd_refresh" class="text-muted mt-2" data-placement="top"
                                                title="Refresh All Feilds">
                                                <i class="fa fa-redo text-dark"></i>
                                            </a>
                                        </div>

                                    </div>

                                    <div class="card-body">

                                        <div class="row">

                                            <div class="col-xl-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Search Item</label>
                                                    <div class="input-group">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-search"></i></span>

                                                        <input type="text" placeholder="Type 'Item Name'"
                                                            class="form-control rounded-end"
                                                            id="item_general_issue_item" />
                                                    </div>
                                                    <input type="hidden" id="item_general_issue_item_id" name="item_general_issue_item"  />
                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="form-group mb-3">
                                                    <label class="form-label">
                                                        Qty
                                                        <span class="text-danger">*</span></label>
                                                    </label>
                                                    <input id="item_general_issue_item_qty" autofocus="autofocus"
                                                        type="number" class="form-control" />
                                                </div>
                                            </div>

                                            <div class="col-xl-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Remark <span
                                                            class="text-danger">*</span></label>
                                                    <textarea class="form-control" id="item_general_issue_item_remark" rows="3">
                                                    </textarea>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="card-footer d-flex flex-row-reverse">
                                        <div class="col-xl-3">
                                            <button id="item_general_issue_item_submit" class="btn btn-primary w-100">
                                                <i class='fa fa-plus'></i> Add Item to Issue
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="col-xl-12">

                                <div class="card mb-3 border-1 shadow-sm">

                                    <div class="card-header bg-dark-400 ">
                                        <h6 class="mt-2 text-white">Added Items for Issuing</h6>
                                    </div>

                                    <div class="card-body">

                                        <div class="row">

                                            <div class="table-responsive">
                                                <table id="item_general_issue_session_table"
                                                    class="tableMheight table table-borderless table-striped text-nowrap pt-2 w-100">
                                                    <thead>
                                                        <tr>
                                                            <th>Item Name</th>
                                                            <th>Item Code</th>
                                                            <th style="">Qty</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>

                                                </table>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="d-flex">

                        <div class="d-flex">

                            <div class="px-1">
                                <button id="item_general_issue_save" class="btn btn-teal">
                                    <i class='fa fa-check'></i>
                                    Save & Complete
                                </button>
                            </div>

                            <div class="px-1">
                                <a class="btn btn-default" id="item_general_delete">
                                    <i class="fa fa-trash"></i>
                                    Delete All</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="itemGeneralIssueModal_view">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-dark-400">

                <h5 class="modal-title header_new_text text-white">REQUESTING NEW GENERAL ITEMS VIEW</h5>

                <div class="d-flex">

                    <div id="printmodal_div" class="px-1">
                        <a id="itemGeneralIssueModalView_printmodal" class="btn btn-sm btn-default btnround"><i
                                class="fa fa-print"></i></a>
                    </div>

                    <div class="px-1 ">
                        <a id="itemGeneralIssueModalView_close" class="btn btn-sm btn-yellow btnround">
                            <i class="far fa-window-minimize"></i>
                        </a>
                    </div>
                </div>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">

                        <div class="row">

                            <div class="col-xl-12 mb-3">
                                <div class="card mb-3 h-100 border-1 shadow-sm">

                                    <div class="card-header bg-gradient-custom-teal border-0">

                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <h6 class="mt-2 text-dark">Primary Details</h6>
                                            </div>
                                            <a id="grnpd_refresh" class="text-muted mt-2" data-placement="top"
                                                title="Refresh All Feilds">
                                                <i class="fa fa-redo text-dark"></i>
                                            </a>
                                        </div>

                                    </div>

                                    <div class="card-body">

                                        <div class="row">

                                            <div class="col-xl-6">
                                                <div class="form-group mb-3">
                                                    <label class="form-label" for="location_id">
                                                        Location
                                                        <span class="text-danger">*</span></label>
                                                    </label>
                                                    <input id="item_general_issue_location_view" type="text" class="form-control" disabled />

                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="form-group mb-3">
                                                    <label class="form-label">
                                                        Manual Code
                                                        <span class="text-danger">*</span></label>
                                                    </label>
                                                    <input id="item_general_issue_manual_code_view" type="text" class="form-control" disabled />
                                                </div>
                                            </div>

                                            <div class="col-xl-12">

                                                <div class="mb-3">
                                                    <label class="form-label">Remark
                                                    </label>
                                                    <textarea id="item_general_issue_remark_view" class="form-control" rows="3" disabled>
                                                    </textarea>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="col-xl-12">

                                <div class="card mb-3 border-1 shadow-sm">

                                    <div class="card-header bg-dark-400 ">
                                        <h6 class="mt-2 text-white">Added Items for Issuing</h6>
                                    </div>

                                    <div class="card-body">

                                        <div class="row">

                                            <div class="table-responsive">
                                                <table id="item_general_issue_session_table_view"
                                                    class="tableMheight table table-borderless table-striped text-nowrap pt-2 w-100">
                                                    <thead>
                                                        <tr>
                                                            <th>Item Name</th>
                                                            <th>Item Code</th>
                                                            <th>Requested Qty</th>
                                                            <th>In Qty</th>
                                                            <th class='w-25'></th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>

                                                </table>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
