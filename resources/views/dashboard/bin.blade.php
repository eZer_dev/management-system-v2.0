@extends('dashboard.layouts.dashboard_app')

@section('content')

<div id="content" class="app-content">
    <div class="container-fluid">

        <div class="row justify-content-center">

            <div class="col-xl-12">

                <div class="row">

                    <div class="col-xl-12">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item header_new_text"><a href="/home">Dashboard</a></li>
                            <li class="breadcrumb-item active header_new_text">BIN Location Management</li>
                        </ul>
                        <h1 class="page-header header_new_text">
                            BIN Location Management
                        </h1>
                        <hr class="mb-4" />

                        <div id="formControls" class="mb-3">

                            <div class="row">

                                <div class="col-xl-12">

                                    <div class="card mb-3">

                                        <div class="card-header">
                                            <div class="d-flex">
                                                <div class="flex-grow-1">
                                                    <h6 class="mt-2">BIN Location Registration</h6>
                                                </div>
                                                <a class="text-muted mt-2" data-toggle="tooltip" data-placement="top"
                                                    title="Refresh All Feilds" onclick=""> <i class="fa fa-redo"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="card-body pb-2">

                                            @include('dashboard.components.flash')

                                            <div class="col-xl-12">
                                                <div class="form-group mb-3">
                                                    <label class="form-label" for="bin_location_id">
                                                        Location
                                                    </label>
                                                    <select id="bin_location_id" name="bin_location_id"
                                                        class="form-select">
                                                        @foreach ($location as $key => $location)
                                                        <option value="{{ $location->id }}">
                                                            {{ $location->location_name }}</option>
                                                        @endforeach
                                                    </select>

                                                    @error('bin_location_id')
                                                    <span class="text-danger">
                                                        <small>{{ $message }}</small>
                                                    </span>
                                                    @enderror

                                                </div>
                                            </div>

                                            <div class="col-xl-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Select Item <span
                                                            class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-search"></i></span>

                                                        <input type="text" placeholder="Type 'ITEM CODE / NAME'"
                                                            class="form-control rounded-end" id="bin_item_code"
                                                            name="bin_item_code" />

                                                        <input type="text" id="bin_item_id" name="bin_item_id"
                                                            hidden="true">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xl-12">
                                                <div class="form-group mb-3">
                                                    <label class="form-label">
                                                        BIN Location Name
                                                    </label>
                                                    <input id="bin_location_name" name="bin_location_name" type="text"
                                                        class="form-control" />
                                                </div>
                                            </div>

                                        </div>

                                        <div class="card-footer">
                                            <div class="col-xl-12">
                                                <button id="bin_save" class="btn btn-primary"><i
                                                        class="fa fa-check"></i> Save BIN</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="d-flex">
                                                <div class="flex-grow-1">
                                                    <h6 class="mt-2">Items List</h6>
                                                </div>

                                                <a type="button" class="text-muted mt-2" title="Print BINs" onclick="bins_print()">
                                                    <i class="fa fa-print"></i>
                                                </a>

                                            </div>
                                        </div>

                                        <div class="card-body table-responsive">
                                            <table id="bin_table"
                                                class=" table table-borderless table-striped text-nowrap pt-2">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Location Name</th>
                                                        <th scope="col">BIN Location Name</th>
                                                        <th scope="col">Item Code</th>
                                                        <th scope="col">Item Name</th>
                                                        <th scope="col">Status</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @include('dashboard.components.bintable')

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
