<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Purchase Order Report</title>

    <style>
        @page {
            size: A4;
            margin: 0;
        }

        @media print {

            html,
            body {
                width: 210mm;
                height: 297mm;
                padding-left: 10px;
                padding-right: 20px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
        }

        .font {
            font-family: 'Segoe UI';
        }

        .text-center {
            text-align: center;
        }

        .row {
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            margin-top: 5px;
        }

        .col-2 {
            width: 16.66%;
        }

        .col-3 {
            width: 25%;
        }

        .col-4 {
            width: 33.33%
        }

        .col-6 {
            width: 50%;
        }

        .tborderth {
            border-top: 1px solid #212121;
            /* border: 1px solid black; */
            padding: 5px;
            margin: 0px;
        }

        .tbleft {
            padding-left: 10px;
            border-left: 1px solid #212121
        }

        .tbright {
            padding-right: 10px;
            border-right: 1px solid #212121
        }

        .tborder {
            /* border-left: 1px solid #212121; */
            /* border-right: 1px solid #212121; */
            /* border-top: 1px solid #212121; */
            border-bottom: 1px solid #212121;
            /* padding: 5px; */
            padding-top: 10px;
            padding-bottom: 10px;
            margin: 0px;
        }

        .tpad {
            padding-top: 10px;
            padding-bottom: 10px;
            margin: 0px;
        }

        .alright {
            text-align: right
        }

        .smargin {
            padding: 5px;
        }

        .bold-100 {
            font-weight: 500;
        }

        .trcolor {
            background-color: #eeeeee;
            -webkit-print-color-adjust: exact;
        }

        .text-align-right {
            margin-left: auto;
            margin-right: 0px;
        }

        .text-center {
            text-align: center;
        }
    </style>

</head>

<body class="font">

    <div class="text-center">
        <h3>TRUST PLASTIC INDUSTRIES PRIVATE LIMITED</h3>
        <span>No. 451/6, Makola North, Makola - 11640</span>
        <h3>Item Transaction REPORT</h3>
    </div>

    <br>

    <div style="padding: 0px">

        <div class="row">
            <div class="col-6">
                <table>
                    <tr>
                        <td><b>Print Date</b></td>
                        <td>&nbsp;</td>
                        <td>01/01/2021</td>
                    </tr>
                </table>
            </div>
        </div>
        <br>



        <div>

            <table class="table-border"
                style="border-spacing: 0; border-width: 0; padding: 0; border-width: 1; width:100%">
                <thead>
                    <tr>
                        <th class="tborderth tbleft tbright" colspan="8">ITEM CODE : GRN00125</th>
                    </tr>

                    <tr class="trcolor ">
                        <th class="tborderth tborder tbleft" style="text-align: left">#</th>
                        <th class="tborderth tborder tbleft">Date</th>
                        <th class="tborderth tborder tbleft">GRN Code</th>
                        <th class="tborderth tborder tbleft">In Qty</th>
                        <th class="tborderth tborder tbleft">Status</th>

                    </tr>
                </thead>
                <tbody>

                    @foreach ($data as $dayKey=>$dayData)
                    @php
                    $index=1;
                    $index1=0;
                    $index2=0;
                    $index3=0;
                    $rowspan=0;
                    if(array_key_exists('grns',$dayData)){
                    $rowspan=$rowspan+count($dayData['grns']);
                    }
                    if(array_key_exists('issues',$dayData)){
                    $rowspan+=count($dayData['issues']);
                    }
                    if(array_key_exists('returns',$dayData)){
                    $rowspan+=count($dayData['returns']);
                    }

                    @endphp

                    @if ($index1==0 && $index2==0)
                    <tr>
                        <td class="tborder tbleft" rowspan="{{  $rowspan }}">{{ $index }}</td>
                        <td class="tborder tbleft text-center" rowspan="{{  $rowspan }}">{{ $dayKey }}</td>
                        @endif



                        @if (array_key_exists('grns',$dayData))

                        @foreach ($dayData['grns'] as $itemData)
                        @if ($index1>0)
                    <tr>
                        @endif
                        <td class="tborder tbleft text-center">{{ $itemData['grn']['grn_code'] }}</td>
                        <td class="tborder tbleft text-center">{{ $itemData['qty'] }}</td>
                        <td class="tborder tbleft text-center">In</td>
                        @if ($index1>0)
                    </tr>
                    @endif
                    @php
                    $index++;
                    $index1++;
                    @endphp
                    @endforeach
                    @endif



                    @if (array_key_exists('issues',$dayData))


                    @foreach ($dayData['issues'] as $itemData)
                    @if ($index2>0)
                    <tr>
                        @endif
                        <td class="tborder tbleft text-center">{{ $itemData['issue']['code'] }}</td>
                        <td class="tborder tbleft text-center">{{ $itemData['qty'] }}</td>
                        <td class="tborder tbleft text-center">Out</td>
                        @if ($index2>0)
                    </tr>
                    @endif
                    @php
                    $index++;
                    $index2++;
                    @endphp
                    @endforeach

                    @endif


                    @if ($index1==0 && $index2==0)
                    </tr>
                    @endif

                    @endforeach
                </tbody>
            </table>

        </div>

    </div>
    <br>

    <div class="text-center row" style="margin-top: 70px">
        <div>
            <span>..............................................</span><br><span><i>Issued by</i></span>
        </div>
    </div>

</body>

</html>
