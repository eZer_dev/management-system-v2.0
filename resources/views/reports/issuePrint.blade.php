<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ISSUE - {{ $data['code'] }}</title>

    <style>
        @page {
            size: A4;
            margin: 0;
        }

        @media print {

            html,
            body {
                width: 210mm;
                height: 297mm;
                padding-left: 10px;
                padding-right: 20px;
                padding-top: 10px;
                padding-bottom: 10px;
            }

        }

        .font {
            font-family: 'Segoe UI';
        }

        .text-center {
            text-align: center;
        }


        .row {
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            margin-top: 5px;
        }

        .col-2 {
            width: 16.66%;
        }


        .col-3 {
            width: 25%;
        }

        .col-4 {
            width: 33.33%
        }

        .col-6 {
            width: 50%;
        }

        .tborderth {
            border-top: 1px solid #212121;
            /* border: 1px solid black; */
            padding: 5px;
            margin: 0px;

        }

        .tbleft {
            padding-left: 10px;
            border-left: 1px solid #212121
        }

        .tbright {
            padding-right: 10px;
            border-right: 1px solid #212121
        }

        .tborder {
            /* border-left: 1px solid #212121; */
            /* border-right: 1px solid #212121; */
            /* border-top: 1px solid #212121; */
            border-bottom: 1px solid #212121;
            /* padding: 5px; */
            padding-top: 10px;
            padding-bottom: 10px;
            margin: 0px;

        }

        .alright {
            text-align: right
        }

        .smargin {
            padding: 5px;
        }

        .bold-100 {
            font-weight: 500;
        }

        .trcolor {
            background-color: #eeeeee;
            -webkit-print-color-adjust: exact;
        }

        .text-align-right {
            margin-left: auto;
            margin-right: 0px;
        }

        .text-center {
            text-align: center;
        }

    </style>

</head>

<body class="font">

    <div class="text-center">
        <h3>TRUST PLASTIC INDUSTRIES PRIVATE LIMITED</h3>
        <span>No. 451/6, Makola North, Makola - 11640</span>
        <br>
        <span><strong>ISSUE REPORT</strong></span>
    </div>

    <br>
    <div style="padding: 0px">

        <div class="row">
            <div class="col-5">
                <table>
                    <tr>
                        <td><b>Code #</b></td>
                        <td>&nbsp;</td>
                        <td>{{ $data['code'] }}</td>
                    </tr>
                    <tr>
                        <td><b>Material Request Code</b></td>
                        <td>&nbsp;</td>
                        <td>{{ $data['materialdata']['mr_code'] }}</td>
                    </tr>
                </table>
            </div>

            <div class="col-5" style="margin-left: auto; margin-right: 0px;">
                <table>

                    <tr>
                        <td><b>Issued By</b></td>
                        <td>&nbsp;</td>
                        <td>{{ ($data['userdata'])?$data['userdata']['fname']:'' }} {{ ($data['userdata'])?$data['userdata']['lname']:'' }}</td>
                    </tr>

                    <tr>
                        <td><b>Issued Date</b></td>
                        <td>&nbsp;</td>
                        <td>{{ $data['created_at']->format('Y-m-d') }}</td>
                    </tr>

                    <!--<tr>-->
                    <!--    <td><b>Print Date</b></td>-->
                    <!--    <td>&nbsp;</td>-->
                    <!--    <td>{{ Carbon\Carbon::now()->format('Y-m-d') }}</td>-->
                    <!--</tr>-->

                </table>
            </div>

        </div>

        <br>
        <br>

        <div>
            <table class="table-border" style="border-spacing: 0; border-width: 0; padding: 0; border-width: 0; width:100%">
                <thead>
                    <tr class="trcolor">
                        <th class="tborderth tborder tbleft bold-100" style="text-align: left">#</th>
                        <th class="tborderth tborder tbleft bold-100" style="text-align: left">Item Code</th>
                        <th class="tborderth tborder tbleft bold-100" style="text-align: left">Part Code</th>
                        <th class="tborderth tborder tbleft bold-100" style="text-align: left">Item Name</th>
                        <th class="tborderth tborder tbleft bold-100 tbright" style="text-align: left">Qty</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $index1=1;
                    @endphp

                    @foreach ($data['issueitems'] as $iitems)
                    <tr>
                        <td class="tborder tbleft">{{ $index1 }}</td>
                        <td class="tborder tbleft" style="text-align: left">{{ $iitems['stockhasitem']['item']['item_code'] }}</td>
                        <td class="tborder tbleft" style="text-align: left">{{ $iitems['stockhasitem']['item']['item_part_code'] }}</td>
                        <td class="tborder tbleft" style="text-align: left">{{ $iitems['stockhasitem']['item']['item_name'] }}</td>
                        <td class="tborder tbleft tbright" style="text-align: left">{{ $iitems['qty'] }}</td>
                    </tr>
                    @php
                    $index1++;
                    @endphp
                    @endforeach

                </tbody>
            </table>

        </div>

        @if (count($data['issuereturnitems'])>0)
        <div>

            <div style="text-align: left">
                <h4>ISSUE RETURN LIST</h4>
            </div>

            <table class="table-border" style="border-spacing: 0; border-width: 0; padding: 0; border-width: 0; width:100%">
                <thead>
                    <tr class="trcolor">
                        <th class="tborderth tborder tbleft bold-100" style="text-align: left">#</th>
                        <th class="tborderth tborder bold-100" style="text-align: left">Item Code</th>
                        <th class="tborderth tborder bold-100" style="text-align: left">Part Code</th>
                        <th class="tborderth tborder bold-100" style="text-align: left">Item Name</th>
                        <th class="tborderth tborder bold-100 tbright" style="text-align: left">Quantity</th>
                    </tr>
                </thead>
                <tbody>

                    @php
                    $index2=1;
                    @endphp

                    @foreach ($data['issuereturnitems'] as $iiitems)
                    <tr>
                        <td class="tborder tbleft">{{ $index2 }}</td>
                        <td class="tborder tbleft" style="text-align: left">{{ $iiitems['stockhasitem']['item']['item_code'] }}</td>
                        <td class="tborder tbleft" style="text-align: left">{{ $iiitems['stockhasitem']['item']['item_part_code'] }}</td>
                        <td class="tborder tbleft" style="text-align: left">{{ $iiitems['stockhasitem']['item']['item_name'] }}</td>
                        <td class="tborder tbleft tbright" style="text-align: left">{{ $iiitems['qty'] }}</td>
                    </tr>
                    @if ($iiitems['remark'])
                    <tr>
                        <td class="tborder tbleft tbright" colspan="5">{{ $iiitems['remark'] }}</td>
                    </tr>
                    @endif
                    @php
                        $index2++;
                    @endphp
                    @endforeach

                </tbody>
            </table>

        </div>
        @endif



        <div style="margin-top: 50px">
            @if ($data['remark'])
            <p style="text-align: justify"><strong>Remark : </strong>{{ $data['remark'] }}</p>
            @endif
        </div>
    </div>
    <br>

</body>

</html>
