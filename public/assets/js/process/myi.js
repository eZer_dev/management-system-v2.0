$('#invoiced_list').DataTable();
// $('#added_material_list').DataTable();
$('#mr_list').DataTable();

// $('#item_general_issue_session_table').DataTable();
$('#item_general_issue_list').DataTable();

$('#invoiceTable').DataTable({
    "searching": false,
    "dom": '<"top"i>rt=<"bottom"flp><"clear">'
});

$('#myi_mr_modal_create').click(function (e) {
    $('#mr_modal_link').modal('toggle');
});

$('#myi_mr_modal_view').click(function (e) {
    $('#mr_view_modal').modal('toggle');
});

$('#itemGeneralIssueModal_close').click(function (e) {
    $('#itemGeneralIssueModal').modal('toggle');
});

function addMaterial(id) {

    $.ajax({
        type: "GET",
        url: "/mr/init",
        data: {
            id: id,
        },
        success: function (response) {

            mr_Table.ajax.reload(null, false);
            $('#mr_selected_prodcut_code').val('');

            $('#mr_created_jobs').html('');

            var row_data = '';
            for (let i = 0; i < response["products"].length; i++) {

                row_data += '<div style="border-bottom:1px solid #e0e0e0">' +
                    '<div class="list-group-item d-flex ps-3 border-0 ">' +
                    '<div class="me-3">' +
                    '<i class="fa fa-cube fa-fw fa-lg" style="color:#212121" ></i>' +
                    '</div>' +
                    '<div class="flex-fill">' +
                    '<div class="font-weight-600 text-dark">' + response["products"][i]['code'] + '</div>' +
                    '<div class="fs-13px text-muted mb-2">' +
                    '<span>' + response["products"][i]['name'] + '</span>' +
                    '<br>' +
                    '<span>Brand : ' + response["products"][i]['brand'] + ' (' + response["products"][i]['model'] + ') ' + '</span>' +
                    '</div>' +
                    '<div class="mb-1">' +
                    '<div class="input-group flex-nowrap">' +
                    '<div>' +
                    '<button class="btn btn-primary btn-sm" onclick="mr_loadProductDetails(' + response["products"][i]['id'] + ')">' +
                    '<i class="fa fa-share-square-o" aria-hidden="true"></i> Add Materials </button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

            }

            $('#mr_code').val(response["mr_code"]);
            $('#mr_job_code').html(response["job_code"]);
            $('#mr_created_jobs').html(row_data);

        }

    });

}

function mr_loadProductDetails(id) {

    $.ajax({
        type: "GET",
        url: "/mr/loadProduct",
        data: {
            id: id
        },
        success: function (response) {

            Notiflix.Notify.Info('Selected ' + response.code + ' product');

            $('#mr_selected_prodcut_code').val(response.code);
            $('#mr_selected_prodcut_id').val(response.id);

        }
    });
}

$('#mr_item_save_session_button').click(function (e) {
    e.preventDefault();

    $.ajax({
        type: "GET",
        url: "/mr/itemSaveSession",
        data: {
            product_id: $('#mr_selected_prodcut_id').val(),
            item_id: $('#mr_item_id').val(),
            qty: $('#mr_item_qty').val(),
        },
        success: function (response) {
            if ($.isEmptyObject(response.error)) {
                mr_clearFields();
                mr_Table.ajax.reload(null, false);
                Notiflix.Notify.Success('Item successfully saved to session ');
            } else {
                $.each(response.error, function (key, value) {
                    Notiflix.Notify.Failure(value);
                });
            }
        }
    });

});

var mr_Table = $('#mr_session_added_list').DataTable({
    ajax: {
        url: '/mr/materialsTableView',
        dataSrc: ''
    },
    createdRow: function (row, data, dataIndex, cells) {
        $(cells).addClass('py-1 align-middle');
    }
});

function mr_removeItemInSession(index) {

    $.ajax({
        type: "GET",
        url: "/mr/removeItemFromSession",
        data: {
            index: index
        },
        success: function (response) {

            if (response == 1) {
                mr_Table.ajax.reload(null, false);
                Notiflix.Notify.Success('Item removed successfully from session');
            } else {
                Notiflix.Notify.Danger('Item removing error');
            }
        }
    });

}

$('#mr_save_to_db_button').click(function (e) {
    e.preventDefault();

    $.ajax({
        type: "GET",
        url: "/mr/saveMaterialRequest",
        success: function (response) {

            if ($.isEmptyObject(response.error)) {

                if (response == 1) {
                    $('#mr_selected_prodcut_code').val('');
                    $('#mr_selected_prodcut_id').val('');
                    mr_clearFields();
                    mr_closeModal();
                    mr_Table.ajax.reload(null, false);
                    jobRecordsDataTable.ajax.reload(null, false);
                    Notiflix.Notify.Success('Successfully saved material request');
                } else if (response == 2) {
                    Notiflix.Notify.Failure('Invalid material request number');
                } else if (response == 3) {
                    Notiflix.Notify.Failure('Please add materials to save');
                }

            } else {
                $.each(response.error, function (key, value) {
                    Notiflix.Notify.Failure(value);
                });
            }
        }
    });

});

$('#mr_fields_delete').click(function (e) {
    e.preventDefault();

    mr_clearFields();

});

//Delete Later
$('#mr_session_product_clear').click(function (e) {
    e.preventDefault();

    Notiflix.Confirm.Show('Notiflix Confirm', 'Do you agree with me?', 'Yes', 'No',
        function () {

            $.ajax({
                type: "GET",
                url: "/mr/productItemSessionClear",
                success: function (response) {
                    mr_clearFields();
                    mr_Table.ajax.reload(null, false);
                    Notiflix.Notify.Success('Material item session successfully cleared ');
                }
            });

        },
        function () {
            Notiflix.Notify.Warning('Ignored material item session remove');
        });



});

function mr_closeModal() {
    $('#mr_modal_link').modal('hide');
    $('#mr_modal_link').modal('toggle');
}

function mr_clearFields() {
    $('#mr_item_code').val('');
    $('#mr_item_id').val('');
    $('#mr_item_qty').val('');
}

var mr_itemsDataArray = {};

var mrItemsTypeHead = $('#mr_item_code').typeahead({
    source: function (query, process) {
        return $.get("/mr/loaditem", {
            query: query,
        }, function (data) {
            mr_itemsDataArray = {};
            data.forEach(element => {
                mr_itemsDataArray[element['name']] = element['id'];
            });
            return process(data);
        });
    },
});

mrItemsTypeHead.change(function (e) {
    var tempId = mr_itemsDataArray[$('#mr_item_code').val()];
    if (tempId != undefined) {
        $('#mr_item_id').val(tempId);
    }
});

function mr_view_modal_on_mr(id) {

    $('#mr_view_job_code').html('');
    $('#mr_view_t_mr_code').html('');
    $('#mr_view_t_job').html('');
    $('#mr_view_t_date').html('');
    $('#mr_view_t_location').html('');
    $('#mr_view_print_btn').attr('');
    $('#mr_view_modal_tbody').html('');

    $.ajax({
        type: "GET",
        url: "/mr/loadRequestedMaterial",
        data: {
            mr_id: id,
        },
        success: function (response) {

            $('#mr_view_job_code').html('#' +
                response['mr_code']);
            $('#mr_view_t_mr_code').html(response['mr_code']);
            $('#mr_view_t_job').html(response['get_jobs']['code']);
            $('#mr_view_t_date').html(response['date']);
            $('#mr_view_t_location').html(response['get_jobs']['locationdata']['location_name']);
            $('#mr_view_print_btn').attr('onclick', 'mr_view_print(' + response['id'] + ')');

            var view = '';

            if (response['status'] != 3) {
                $('#mr_view_approval_section_mf').removeClass('modal-footer');
                $('#mr_view_approval_section').html('');
            } else {

                $('#mr_view_approval_section_mf').addClass('modal-footer');

                view =
                    '<div class="px-1">' +
                    '<button id = "mr_view_approve_button" class="btn btn-yellow" ' +
                    ' onclick="mr_view_approval(' + response['id'] + ',' +
                    1 + ' )">' +
                    '<i class="fa fa-check"> </i>Approve</button>' +
                    '</div>' +
                    '<div class="px-1">' +
                    '<button id="mr_view_refuse_button" class="btn btn-danger" ' +
                    ' onclick="mr_view_approval(' + response['id'] + ',' +
                    2 + ' )">' +
                    '<i class="fa fa-close"> </i>Refuse</button>' +
                    '</div>';

                $('#mr_view_approval_section').html(view);
            }

            $data = '';

            for (let i = 0; i < response['get_all_materials'].length; i++) {

                x = i + 1;

                $data += '<tr>' +
                    '<td > ' + x + ' </td>' +
                    '<td>' + response['get_all_materials'][i]['getjob_has_products']['code'] + '</td>' +
                    '<td>' + response['get_all_materials'][i]['getjob_has_products']['name'] + '</td>' +
                    '<td> ' + response['get_all_materials'][i]['get_item_by_id']['item_name'] + ' </td>' +
                    '<td> ' + response['get_all_materials'][i]['qty'] + ' ' + response['get_all_materials'][i]['get_item_by_id']['munit']['symbol'] + ' </td>' +
                    '</tr > ';
            }

            $('#mr_view_modal_tbody').html($data);

        }
    });

}

function mr_view_approval(id, status) {

    var doProceed = false;

    if (status == 1) {
        Notiflix.Confirm.Show('Material Approval Confirmation', 'Please confirm to proceed?', 'Approve', 'Cancel', function () {
            doProceed = true;
            mr_view_approval_process(id, status, doProceed);
        });
    } else if (status == 2) {
        Notiflix.Confirm.Show('Material Refuse Confirmation', 'Please confirm to proceed?', 'Refuse', 'Cancel', function () {
            doProceed = true;
            mr_view_approval_process(id, status, doProceed);
        });
    }

}

function mr_view_approval_process(id, status, check) {

    if (check == true) {
        $.ajax({
            type: "GET",
            url: "/mr/mrApproval",
            data: {
                mr_id: id,
                approval_status: status,
            },
            success: function (response) {
                Notiflix.Notify.Success('Changed status');
                setInterval(function () {
                    location.reload()
                }, 750);
            }
        });
    }
}

function mr_view_print(id) {

    $.ajax({
        type: "GET",
        url: "/mr/printRequestedMaterial",
        data: {
            mr_id: id
        },
        success: function (response) {
            if (response == 2) {
                Notiflix.Notify.Warning('Something Wrong.');
            } else {
                printReport(response);
            }
        }


    });
}

var invoice_productArray = {};

var invoiceTypeHead = $('#invoice_product_code').typeahead({
    source: function (query, process) {
        return $.get("/invoices/loadProducts", {
            query: query,
        }, function (data) {
            invoice_productArray = {};
            data.forEach(element => {
                invoice_productArray[element['name']] = element['id'];
            });
            return process(data);
        });
    },
});

invoiceTypeHead.change(function (e) {
    var tempId = invoice_productArray[$('#invoice_product_code').val()];
    if (tempId != undefined) {
        $('#invoice_product_id').val(tempId);
        viewProductQtyCount(tempId);
    }
});

$('#invoice_location_id').change(function (e) {
    e.preventDefault();

    if ($('#invoice_product_id').val() !== '') {
        viewProductQtyCount();
    }

});

$('#invoice_product_clear_btn').click(function (e) {
    e.preventDefault();
    invoice_clear_product_fields();
});

$('#invoice_location_id').change(function (e) {
    e.preventDefault();

    $('#invoice_location_id').attr('disabled', 'true');

});

function invoice_clear_product_fields() {
    $('#invoice_product_code').val('');
    $('#invoice_unit_price').val('');
    $('#invoice_qty').val('');
    $('#invoice_qty').removeAttr('placeholder');
}

function viewProductQtyCount() {
    $.ajax({
        type: "GET",
        url: "/invoices/getProductQtyCount",
        data: {
            product_id: $('#invoice_product_id').val(),
            location_id: $('#invoice_location_id').val(),
        },
        success: function (response) {
            if (response == 0) {
                Notiflix.Notify.Failure('Product is out of stock');
            } else {
                Notiflix.Notify.Warning('Available Quantity: ' + response);
                $('#invoice_qty').attr('placeholder', 'Available Quantity: ' + response);
            }
        }
    });
}

$('#invoice_product_insert_to_session_btn').click(function (e) {
    e.preventDefault();

    if ($('#invoice_unit_price ').val() <= 0 || $('#invoice_qty').val() <= 0) {
        if ($('#invoice_unit_price ').val() <= 0) {
            Notiflix.Notify.Failure('Invalid Unit Price');
            $('#invoice_unit_price ').val('')
        } else {
            Notiflix.Notify.Failure('Invalid Product Quantity');
            $('#invoice_qty').val('')
        }
    } else {

        $.ajax({
            type: "GET",
            url: "/invoices/productAddSession",
            data: {
                location_id: $('#invoice_location_id').val(),
                product_id: $('#invoice_product_id').val(),
                unit_price: $('#invoice_unit_price').val(),
                qty: $('#invoice_qty').val(),
            },
            success: function (response) {
                if ($.isEmptyObject(response.error)) {
                    if (response == 'RESP1') {
                        Notiflix.Notify.Failure('This product does not have any stock');
                    } else if (response == 'RESP2') {
                        Notiflix.Notify.Failure('Invalid stock');
                    } else {
                        $('#invoice_sub_tot').val('LKR ' + response);
                        Notiflix.Notify.Success('Product add to invoice list successfully');
                        invoice_clear_product_fields();
                        invoice_Table.ajax.reload(null, false);
                        invoice_netTotal();
                    }

                } else {
                    $.each(response.error, function (key, value) {
                        Notiflix.Notify.Failure(value);
                    });
                }
            }
        });
    }
});

$('#invoice_dis').change(function (e) {
    e.preventDefault();
    invoice_netTotal();
});

$('#invoice_vat').change(function (e) {
    e.preventDefault();
    invoice_netTotal();
});

function invoice_netTotal() {

    var discount = $('#invoice_dis').val();
    var vat = $('#invoice_vat').val();

    if (discount === '') {
        discount = 0;
    }

    if (vat === '') {
        vat = 0;
    }

    $.ajax({
        type: "GET",
        data: {
            discount: discount,
            vat: vat,
        },
        url: "/invoices/getInvoiceNettotal",
        success: function (response) {
            $('#invoice_nettotal').val('LKR ' + response);
        }
    });

}

function invoice_removeItem_from_list(index) {

    if (index != null) {
        $.ajax({
            type: "GET",
            url: "/invoices/removeFromSession",
            data: {
                index: index,
            },
            success: function (response) {
                console.log(response);
                if (response != '2') {
                    $('#invoice_sub_tot').val('LKR ' + response['subtotal']);
                    invoice_netTotal();
                    Notiflix.Notify.Success('Product from invoice list successfully');
                    invoice_Table.ajax.reload(null, false);
                }
            }
        });
    }
}

$('#invoice_save_btn').click(function (e) {
    e.preventDefault();

    $(this).prop("disabled", true);
    $('#invoice_and_print_save_btn').prop("disabled", true);

    let ic = $('#invoice_code').val();
    let date = $('#invoice_date').val();
    let location = $('#invoice_location_id').val();
    let remark = $('#invoice_remark').val();
    let po = $('#invoice_po').val();
    let bt = $('#invoice_bt').val();
    let ba = $('#invoice_ba').val();
    let dis = $('#invoice_dis').val();
    let vat = $('#invoice_vat').val();

    $.ajax({
        type: "GET",
        url: "/invoices/invoiceSave",
        data: {
            code: ic,
            date: date,
            location: location,
            remark: remark,
            po: po,
            bt: bt,
            ba: ba,
            dis: dis,
            vat: vat,
        },
        success: function (response) {
            $('#invoice_save_btn').prop("disabled", false);
            $('#invoice_and_print_save_btn').prop("disabled", false);
            if ($.isEmptyObject(response.error)) {
                if (response == 1) {
                    Notiflix.Notify.Failure('Please add products to list and retry');
                } else {
                    Notiflix.Notify.Success('Saved Invoice Successfully');
                    invoice_Table.ajax.reload(null, false);
                    $('#invoice_code').val(response[0]);
                    invoice_delete_vals();

                    Notiflix.Confirm.Show('Invoice Print Confirmation', 'Do you want to get a print?', 'Yes', 'No', function () {
                        invoice_print(response[1]);
                    }, function () {});


                }
            } else {
                $.each(response.error, function (key, value) {
                    Notiflix.Notify.Failure(value);
                });
            }
        },

    });
});

function invoice_print(invoice_id) {
    // function invoice_print() {
    // invoice_id = 129;
    $.ajax({
        type: "GET",
        url: "/invoices/printInvoice",
        data: {
            invoice_id: invoice_id
        },
        success: function (response) {
            if (response == 2) {
                Notiflix.Notify.Warning('Something Wrong.');
            } else {
                printReport(response);
            }
        }
    });
}

$('#resetinvoice').click(function (e) {
    e.preventDefault();

    Notiflix.Confirm.Show('Values Reset Confirm', 'Do you want to clear entered values?', 'Yes', 'No', function () {
        invoice_delete_vals();
    }, function () {});

});

function invoice_delete_vals() {
    $('#invoice_date').val('');
    $('#invoice_location_id').val(1);
    $('#invoice_location_id').prop("disabled", false);;
    $('#invoice_remark').val('');
    $('#invoice_po').val('');
    $('#invoice_bt').val('');
    $('#invoice_ba').val('');
    $('#invoice_dis').val('0');
    $('#invoice_vat').val('0');
}

var invoice_Table = $('#invoice_session_table').DataTable({
    ajax: {
        url: '/invoices/invoiceTableView',
        dataSrc: ''
    },
    createdRow: function (row, data, dataIndex, cells) {
        $(cells).addClass('py-1 align-middle');
    }
});

$('#myi_mr_modal_view').click(function (e) {
    $('#mr_view_modal').modal('toggle');
});

var bin_itemsDataArray = {};

var binItemsTypeHead = $('#bin_item_code').typeahead({
    source: function (query, process) {
        return $.get("/bin/loaditem", {
            query: query,
        }, function (data) {
            bin_itemsDataArray = {};
            data.forEach(element => {
                bin_itemsDataArray[element['name']] = element['id'];
            });
            return process(data);
        });
    },
});

binItemsTypeHead.change(function (e) {
    var tempId = bin_itemsDataArray[$('#bin_item_code').val()];
    if (tempId != undefined) {
        $('#bin_item_id').val(tempId);
    }
});

$('#bin_table').DataTable();

$('#bin_save').click(function (e) {
    e.preventDefault();

    $.ajax({
        type: "GET",
        url: "/bin/savebin",
        data: {
            location_id: $('#bin_location_id').val(),
            bin_name: $('#bin_location_name').val(),
            item_id: $('#bin_item_id').val(),
        },
        success: function (response) {

            if ($.isEmptyObject(response.error)) {
                Notiflix.Notify.Success('Successfully Saved');
                setInterval(function () {
                    location.reload()
                }, 750);
            } else {
                $.each(response.error, function (key, value) {
                    Notiflix.Notify.Failure(value);
                });
            }

        }
    });

});

function binStatusChange(id, status) {
    $.ajax({
        type: "GET",
        url: "/bin/changeStatus",
        data: {
            id: id,
            status: status,
        },
        success: function (response) {

            Notiflix.Notify.Success('Successfully Change Status');
            setInterval(function () {
                location.reload()
            }, 750);
        }
    });
}

function bins_print() {
    Notiflix.Confirm.Show('Print Confirmation', 'Please confirm to proceed print', 'Proceed', 'Cancel', function () {

        $.ajax({
            type: "GET",
            url: "/bin/printAllBins",
            success: function (response) {
                if (response == 2) {
                    Notiflix.Failure('Something wrong on BIN report')
                } else {
                    printReport(response);
                }
            }
        });

    }, function () {});
}


function invoice_loadModalforView(invoice_id) {

    $.ajax({
        type: "GET",
        url: "/invhistory/viewSavedInvoice",
        data: {
            invoice_id: invoice_id,
        },
        success: function (response) {

            if (response != 1) {

                $('#invoice_viewCode').html(response['invoice_code']);
                $('#invoice_viewDate').html(response['invoice_date']);
                $('#invoice_viewLocation').html(response['invoice_location']);
                $('#invoice_viewPo').html(response['invoice_po_no']);
                $('#invoice_viewBillingto').html(response['invoice_bt']);
                $('#invoice_viewBillingtoaddress').html(response['invoice_ba']);
                $('#invoice_viewSubtotal').html(response['invoice_subtotal']);
                $('#invoice_viewDiscount').html(response['invoice_discount']);
                $('#invoice_viewVat').html(response['invoice_vat']);
                $('#invoice_viewNettotal').html(response['nettotal']);
                $('#invoice_viewRemark').html(response['invoice_remark']);

                $('#invoice_viewModelPrint').attr('onclick', 'invoice_print(' + invoice_id + ')');

                invoice_productviewtable.ajax.reload(null, false);

                $('#invoice_viewModel').modal('toggle');

            } else {
                Notiflix.Notify.Failure('Unable to load data')
            }

        }
    });


}

var invoice_productviewtable = $('#invoice_productviewtable').DataTable({
    
    columnDefs: [{
        targets: 3,
        className: 'dt-body-right'
    }],
    
    ajax: {
        url: '/invhistory/viewSavedInvoiceProductList',
        dataSrc: ''
    },
    processing: true,
    language: {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading...'
    },
    createdRow: function (row, data, dataIndex, cells) {
        $(cells).addClass('py-1 align-middle');
    }
});

function invoice_closeModal() {
    $('#invoice_viewModel').modal('hide');
    $('#invoice_viewModel').modal('toggle');
}

var generalIssuesItemTempMap = {};
var generalIssuesItemTempMap = $('#item_general_issue_item').typeahead({
    source: function (query, process) {
        return $.get('/items_general_issues/get/suggetions', {
            query: query,
        }, function (data) {
            generalIssuesItemTempMap = {};
            data.forEach(element => {
                generalIssuesItemTempMap[element['name']] = element['id'];
            });
            return process(data);
        });
    }
});

generalIssuesItemTempMap.change(function (e) {
    var tempId = generalIssuesItemTempMap[$('#item_general_issue_item').val()];
    if (tempId != undefined) {
        $('#item_general_issue_item_id').val(tempId);
    }
});

$('#item_general_issue_item').keyup(function (e) {
    if ($(this).val().length == 0) {
        $('#item_general_issue_item_id').val("");
    }
});

var item_general_issue_item_Table = $('#item_general_issue_session_table').DataTable({
    ajax: {
        url: '/items_general_issues/session/data',
        dataSrc: ''
    },
    createdRow: function (row, data, dataIndex, cells) {
        $(cells).addClass('py-1 align-middle');
    }
});

$('#item_general_issue_item_submit').click(function (e) {
    e.preventDefault();

    $.ajax({
        type: "GET",
        url: "/items_general_issues/session/save",
        data: {
            item_id: $('#item_general_issue_item_id').val(),
            qty: $('#item_general_issue_item_qty').val(),
            remark: $('#item_general_issue_item_remark').val(),
        },
        success: function (response) {
            
            console.log(response);

            if ($.isEmptyObject(response.error)) {

                $('#item_general_issue_item').val("")
                $('#item_general_issue_item_id').val("")
                $('#item_general_issue_item_qty').val("");
                $('#item_general_issue_item_remark').val("");

                $('#item_general_issue_item').focus();

                item_general_issue_item_Table.ajax.reload(null, false);

            } else {

                $.each(response.error, function (key, value) {
                    Notiflix.Notify.Failure(value);
                });

            }

        }
    });

});

function removeGeneralIssueItem_session(index) {

    if (index != null) {
        $.ajax({
            type: "GET",
            url: "/items_general_issues/session/remove",
            data: {
                index: index,
            },
            success: function (response) {

                if (response == 1) {
                    Notiflix.Notify.Success('Successfully item remove from general issuing list');
                    item_general_issue_item_Table.ajax.reload(null, false);
                }
            }
        });
    }

}

$('#item_general_issue_save').click(function (e) {
    e.preventDefault();

    $.ajax({
        type: "GET",
        url: "/items_general_issues/db/save",
        data: {
            location_id: $('#item_general_issue_location').val(),
            manual_id: $('#item_general_issue_manual_code').val(),
            gi_remark: $('#item_general_issue_remark').val(),
        },

        success: function (response) {

            if ($.isEmptyObject(response.error)) {

                if (response == 1) {
                    Notiflix.Notify.Failure('Please insert requested items before save');
                }

                if (response == 2) {
                    Notiflix.Notify.Success('Request saved successfully');

                    $('#item_general_issue_manual_code').val(''),
                        $('#item_general_issue_remark').val(''),

                        item_general_issue_item_Table.ajax.reload(null, false);
                    item_general_issue_requested_list.ajax.reload(null, false);

                    $('#itemGeneralIssueModal').modal('toggle');

                }

            } else {

                $.each(response.error, function (key, value) {
                    Notiflix.Notify.Failure(value);
                });

            }

        }

    });

});


function approval_GeneralIssue(id) {

    Notiflix.Confirm.Show('General Request Confirm', 'Do you want to approval request?', 'Approve', 'Cancel', function () {
        approval_request(1, id);
    }, function () {});

}

function refuse_GeneralIssue(id) {

    Notiflix.Confirm.Show('General Request Confirm', 'Do you want to refuse request?', 'Refuse', 'Cancel', function () {
        approval_request(0, id);
    }, function () {});

}

function approval_request(status, id) {

    $.ajax({
        type: "GET",
        url: "/items_general_issues/issue/approval",
        data: {
            id: id,
            status: status,
        },
        success: function (response) {

            if ($.isEmptyObject(response.error)) {

                if (response == 1) {
                    Notiflix.Notify.Success('Request Issue Successfully Approved');
                } else if (response == 2) {
                    Notiflix.Notify.Info('Request Issue Successfully Refused');
                }

                item_general_issue_requested_list.ajax.reload(null, false);

            } else {

                $.each(response.error, function (key, value) {
                    Notiflix.Notify.Failure(value);
                });

            }
        }
    });

}

var item_general_issue_requested_list = $('#item_general_issue_requested_list').DataTable({
    ajax: {
        url: '/items_general_issues/view/table',
        dataSrc: ''
    },
    processing: true,
    language: {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading...'
    },
    createdRow: function (row, data, dataIndex, cells) {
        $(cells).addClass('py-1 align-middle');
    }
});

$('#item_GeneralIssue').click(function (e) {
    e.preventDefault();

    item_general_issue_requested_list.ajax.reload(null, false);

});

var item_general_issue_session_table_view = $('#item_general_issue_session_table_view').DataTable({

    ajax: {
        url: '/items_general_issues/saved/view/table',
        dataSrc: ''
    },
    createdRow: function (row, data, dataIndex, cells) {
        $(cells).addClass('py-1 align-middle');
    }

});

function itemIssue4_GeneralIssue(id) {

    $.ajax({
        type: "GET",
        url: "/items_general_issues/saved/view/session",
        data: {
            id: id,
        },
        success: function (response) {
            item_general_issue_session_table_view.ajax.reload(null, false);
        }
    });

    $('#itemGeneralIssueModal_view').modal('toggle');

}

$('#itemGeneralIssueModalView_close').click(function (e) {
    e.preventDefault();

    $('#itemGeneralIssueModal_view').modal('toggle');

});

function print_GeneralIssue(id) {

    $.ajax({
        type: "GET",
        url: "/items_general_issues/saved/view/report",
        data: {
            id: id,
        },
        success: function (response) {

            if (response == 1) {
                Notiflix.Notify.Warning('Unable to print');
            } else {
                printReport(response);
            }

        }
    });

}

function qty_reseteGeneralIssueItem_view(index) {
    $(index).val('');
}

function qty_updateGeneralIssueItem_db(index, id) {

    qty_value = $(index).val();

    if ($(index).val() > 0) {

        Notiflix.Confirm.Show('Item Issue Confirmation', 'Please Confirm to issue following item', 'Confirm', 'Cancel', function () {

            $.ajax({
                type: "GET",
                url: "/items_general_issues/saved/qty/update",
                data: {
                    id: id,
                    value: qty_value,
                },
                success: function (response) {

                    if ($.isEmptyObject(response.error)) {

                        if (response['code'] == 2) {

                            Notiflix.Report.Failure(
                                response['des'][0],
                                response['des'][1],
                                response['des'][2],
                            );

                        } else if (response['type'] == 'success') {
                            Notiflix.Notify.Success(response['des']);
                        } else if (response['type'] == 'error') {
                            Notiflix.Notify.Failure(response['des']);
                        }

                        if (response['feild_reset_status'] == 1) {
                            $(index).val('');
                        }

                        if (response['refresh_status'] == 1) {
                            item_general_issue_requested_list.ajax.reload(null, false);
                            $('#itemGeneralIssueModal_view').modal('toggle');
                        }

                    } else {

                        $.each(response.error, function (key, value) {
                            Notiflix.Notify.Failure(value);
                        });

                    }
                }
            });

        }, function () {
            Notiflix.Notify.Warning('Ignored the Confirmation');
        });

    } else {
        Notiflix.Notify.Failure('Invalid Qty');
        $(index).val('');
    }

}
