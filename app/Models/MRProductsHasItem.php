<?php

namespace App\Models;

use App\Models\item;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MRProductsHasItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_has_product_id',
        'material_request_id',
        'item_id',
        'qty',
        'status',
        'available',
    ];

    public function add($data)
    {
        return $this->create($data);
    }

    public function getjobHasProducts()
    {
        return $this->hasOne(Product::class, 'id', 'job_has_product_id');
    }

    public function getItemById()
    {
        return $this->hasOne(Item::class, 'id', 'item_id')->with('munit');
    }

    public function items()
    {
        return $this->hasOne(Item::class, 'id', 'item_id')->with('munit');
    }

    public function getMaterialRequestItemsByMRID($mrid)
    {
        return $this::where('material_request_id', $mrid)->where('available', '>', 0)->selectRaw("id,job_has_product_id,material_request_id,item_id,available,SUM(available) as totqty")
            ->groupBy('item_id')->with('items')->get();
    }

    public function getMaterialRequestItemByMRIID($itemid)
    {
        return $this::where('item_id', $itemid)->selectRaw("item_id,available,SUM(available) as totqty")
        ->groupBy('item_id')->first();
    }
}
