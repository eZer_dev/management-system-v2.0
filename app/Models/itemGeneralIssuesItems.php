<?php

namespace App\Models;

use App\Http\Controllers\SessionActivityController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class itemGeneralIssuesItems extends Model
{
    use HasFactory;

    protected $fillable = ['itemGeneralIssue_id', 'item_id', 'qty', 'in_qty', 'remark', 'status'];

    public function getAll()
    {
        return $this::orderby('id')->orderBy('id', 'DESC')->get();
    }

    public function getItemCount()
    {
        return $this::count();
    }

    public function add($data)
    {
        (new SessionActivityController)->createActivity(['view' => 'General Item Request item saved', 'activity' => 'Added']);
        return $this->create($data);
    }

    public function getItem()
    {
        return $this->hasOne(item::class, 'id', 'item_id')->with('munit');
    }

    

}
