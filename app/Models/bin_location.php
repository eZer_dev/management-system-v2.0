<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bin_location extends Model
{
    use HasFactory;

    protected $fillable = ['location_id','item_id','bin_location_name','status'];

    public function add($data)
    {
        return $this->create($data);
    }

    public function getAll()
    {
        return $this::orderBy('location_id','ASC')->orderBy('id','DESC')->get();
    }

    public function edit($key, $term, $data)
    {
        return $this->where($key, $term)->update($data);
    }

    public function getBinLocationById($id)
    {
        return $this::where('id', $id)->first();
    }

    public function getBinLocationByCode($code)
    {
        return $this::where('bin_location_name', $code)->first();
    }

    public function getLocationBins($lid)
    {
        return $this::where('location_id', $lid)->where('status', 1)->get();
    }

    public function getBinLocationByItemAndLocation($id,$to)
    {
        return $this::where('item_id', $id)->with('item')->where('location_id',$to)->get();
    }

    public function item()
    {
        return $this->hasOne(item::class,'id','item_id')->with('munit');
    }

    public function getLocationBinsByProduct($lid,$pid)
    {

        $productBins=[];

        $locationBins=[];

        foreach ($this::where('location_id', $lid)->where('status', 1)->get() as $keyBins => $valueBins) {
            $locationBins[]=$valueBins->id;
        }

        foreach (JobHasProduct::where('product',$pid)->whereIn('bin',$locationBins)->with('bindata')->get() as $key => $value) {
            $productBins[]=$value['bindata'];
        }

        $existsArr=[];
            foreach (JobHasProduct::get() as $key1 => $value1) {
                $existsArr[]=$value1->bin;
            }

            foreach ($this::where('location_id',$lid)->whereNotIn('id',$existsArr)->get() as $key2 => $value2) {
                $productBins[]=$value2;
            }

        return $productBins;
    }
}
