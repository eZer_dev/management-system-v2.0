<?php

namespace App\Models;

use App\Http\Controllers\SessionActivityController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class itemGeneralIssuesRecords extends Model
{
    use HasFactory;

    protected $fillable = ['generalIssue_id', 'generalIssueItem_id', 'stock_id', 'item_id', 'bin_location_id', 'qty', 'status'];

    public function add($data)
    {
        (new SessionActivityController)->createActivity(['view' => 'General Item Request Record', 'activity' => 'Added']);
        return $this->create($data);
    }

}
