<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class issuingitems extends Model
{
    use HasFactory;

    protected $fillable=['issuing','shi','qty'];

    function add ($data) {
        return $this->create($data);
    }

    public function stockhasitem()
    {
        return $this->hasOne(StockHasItems::class, 'id', 'shi')->with('item')->with('bindata');
    }

    public function issue()
    {
        return $this->hasOne(issuing::class,'id','issuing');
    }
}
