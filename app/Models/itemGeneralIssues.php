<?php

namespace App\Models;

use App\Http\Controllers\SessionActivityController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class itemGeneralIssues extends Model
{
    use HasFactory;

    protected $fillable = ['generalIssue_code', 'location_id', 'manual_id', 'remark', 'status'];

    public function getAll()
    {
        return $this::orderBy('created_at', 'DESC')->get();
    }

    public function getItemCount()
    {
        return $this::count();
    }

    public function add($data)
    {
        (new SessionActivityController)->createActivity(['view' => 'General Item Request', 'activity' => 'Added']);
        return $this->create($data);
    }

    public function generalItems()
    {
        return $this->hasMany(itemGeneralIssuesItems::class, 'itemGeneralIssue_id', 'id')->with('getItem');
    }

    public function location(){
        return $this->hasOne(location::class, 'id', 'location_id');
    }

    public function printReport($id)
    {
        // return $this::where('id',$id)->with('generalItems')->with('location')->with('supplier')->first();
        return $this::where('id', $id)->with('generalItems')->with('location')->first();
    }

    public function getItemQty($id, $location_id){

        return (new StockHasItems)->getItemsForGeneralIssues($id, $location_id)->selectRaw("SUM(qty) as totqty")->first();

    }

    public function getGeneralItems($id){
        return $this::where('id', $id)->with('generalItems')->first();
    }

}
