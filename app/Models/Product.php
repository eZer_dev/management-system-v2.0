<?php

namespace App\Models;

use App\Http\Controllers\SessionActivityController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['vehicle', 'code', 'name', 'status'];

    public function getNextId()
    {
        return DB::select("SHOW TABLE STATUS LIKE 'products'")[0]->Auto_increment;
    }

    public function createProduct($data, $activity)
    {
        (new SessionActivityController)->createActivity($activity);
        return $this->create($data);
    }

    public function getAll($status = null)
    {
        return ($status == null) ? $this::orderBy('id','DESC')->get() : $this::where('status', $status)->with('vehicles')->orderBy('id','DESC')->get();
    }

    public function edit($id, $data, $activity)
    {
        (new SessionActivityController)->createActivity($activity);
        return $this->where('id', $id)->update($data);
    }

    public function getData($id)
    {
        return $this->where('id', $id)->first();
    }

    public function vehicles()
    {
        return $this->hasOne(Vehicle::class, 'id', 'vehicle');
    }

    public function suggetionsByAll($input)
    {
        return $this::where([
            ['status', '=', 1],
            ["name", "LIKE", "%{$input['query']}%"],
        ])->orWhere([
            ['status', '=', 1],
            ["code", "LIKE", "%{$input['query']}%"],
        ])->get();
    }

    public function suggetions($vid,$input)
    {
        return $this::where('vehicle',$vid)->where([
            ['status', '=', 1],
            ['vehicle', '=', $vid],
            ["name", "LIKE", "%{$input['query']}%"],
        ])->orWhere([
            ['status', '=', 1],
            ['vehicle', '=', $vid],
            ["code", "LIKE", "%{$input['query']}%"],
        ])->get();
    }

    public function suggetions_to_invoice($input)
    {
        return $this::where([
            ['status', '=', 1],
            ["name", "LIKE", "%{$input['query']}%"],
        ])->orWhere([
            ['status', '=', 1],
            ["code", "LIKE", "%{$input['query']}%"],
        ])->with('vehicles')->get();
    }

    public function getProductIDSByVehicle($vid)
    {
        $ids=[];

        foreach ($this::where('vehicle',$vid)->get() as $key => $value) {
            $ids[]=$value->id;
        }

        return $ids;
    }
}
