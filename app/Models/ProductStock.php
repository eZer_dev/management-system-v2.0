<?php

namespace App\Models;

use App\Http\Controllers\SessionActivityController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    use HasFactory;

    protected $fillable = ['job', 'location', 'status'];

    public function addRecord($data, $activity)
    {
        (new SessionActivityController)->createActivity($activity);
        return $this->create($data);
    }

    public function getData($data, $binWise = false)
    {
        $query = ProductStockHasProducts::where('status', 1);

        if ($data['from'] != null) {
            $query->whereDate('created_at', '>=', Carbon::parse($data['from']));
        }

        if ($data['to'] != null) {
            $query->whereDate('created_at', '<=', Carbon::parse($data['to']));
        }

        if ($data['bin'] != null) {
            $query = $query->where('bin', $data['bin']);
        }

        if ($data['product'] != null) {
            $query = $query->where('product', $data['product']);
        }

        if ($data['vehicle'] != null) {
            $query = $query->whereIn('product', (new Product)->getProductIDSByVehicle($data['vehicle']));
        }

        if ($data['location'] != null) {
            $query = $query->whereIn('productstock', $this->getProductIDSByLocation($data['location']));
        }

        if ($data['job'] != null) {
            $query = $query->whereIn('productstock', $this->getProductIDSByJob($data['job']));
        }

        if ($binWise == false) {
            return $query->selectRaw("product,SUM(qty) as totqty")
                ->groupBy('product')->orderBy('id', 'DESC')->with('productdata')->get();
        } else {
            return $query->selectRaw("product,bin,SUM(qty) as totqty")
                ->groupBy('bin')->orderBy('id', 'DESC')->with('productdata')->get();
        }
    }

    public function getProductIDSByLocation($lid)
    {
        $ids=[];

        foreach ($this::where('location',$lid)->get() as $key => $value) {
            $ids[]=$value->id;
        }

        return $ids;
    }

    public function getProductIDSByJob($id)
    {
        $ids=[];

        foreach ($this::where('job',$id)->get() as $key => $value) {
            $ids[]=$value->id;
        }

        return $ids;
    }

    public function hasjob()
    {
        return $this->hasOne(Job::class,'id', 'job');
    }

}
