<?php

namespace App\Models;

use App\Http\Controllers\SessionActivityController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductStockHasProducts extends Model
{
    use HasFactory;

    protected $fillable = ['productstock', 'product', 'bin', 'qty', 'nettotal', 'status'];

    public function addRecord($data, $activity)
    {
        (new SessionActivityController)->createActivity($activity);
        return $this->create($data);
    }

    public function productdata()
    {
        return $this->hasOne(Product::class, 'id', 'product');
    }

    public function getProductStocksForInvoice($product_id, $location_id)
    {
        $binIds = [];

        foreach ((new bin_location)->getLocationBins($location_id) as $key => $value) {
            $binIds[] = $value->id;
        }
        return ProductStockHasProducts::where('product', $product_id)->where('status', 1)->whereIn('bin', $binIds)->orderBy('created_at', 'ASC');
    }

    public function pstock()
    {
        return $this->hasOne(ProductStock::class, 'id', 'productstock')->with('hasjob');
    }

    public function getJobByProductId($phs_id)
    {
        return $this->where('id', $phs_id)->with('pstock');
    }

    public function getJobByProductIdforInvoice()
    {
        return $this->hasOne(ProductStockHasProducts::class, 'id', 'phs_id');
    }

}
