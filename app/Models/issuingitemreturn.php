<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class issuingitemreturn extends Model
{
    use HasFactory;

    protected $fillable=['user','issuing','shi','qty','remark','status'];

    public function stockhasitem()
    {
        return $this->hasOne(StockHasItems::class, 'id', 'shi')->with('item')->with('bindata');
    }

    public function issue()
    {
        return $this->hasOne(issuing::class,'id','issuing');
    }
}
