<?php

namespace App\Models;

use App\Http\Controllers\SessionActivityController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class issuing extends Model
{
    use HasFactory;

    protected $fillable=['code','remark','mrequest','user','status'];

    public function getNextId()
    {
        return DB::select("SHOW TABLE STATUS LIKE 'issuings'")[0]->Auto_increment;
    }

    function add ($data, $activity) {
        (new SessionActivityController)->createActivity($activity);
        return $this->create($data);
    }

    public function getData($sdate,$edate)
    {
        $query = $this::where('status', 1);

        if ($sdate != 0) {
            $query->whereDate('created_at', '>=', Carbon::parse($sdate));
        }

        if ($edate != null) {
            $query->whereDate('created_at', '<=', Carbon::parse($edate));
        }

        return $query->with('materialdata')->with('userdata')->orderBy('id','DESC')->get();
    }

    public function userdata()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    public function materialdata()
    {
        return $this->hasOne(material_request::class, 'id', 'mrequest');
    }

   public function issueitems()
   {
       return $this->hasMany(issuingitems::class, 'issuing', 'id')->with('stockhasitem');
   }

   public function issuereturnitems()
   {
       return $this->hasMany(issuingitemreturn::class, 'issuing', 'id')->with('stockhasitem');
   }

   public function getIssue($iid)
   {
       return $this::where('id',$iid)->with('issueitems')->with('issuereturnitems')->with('userdata')->with('materialdata')->first();
   }
}
