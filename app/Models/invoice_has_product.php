<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class invoice_has_product extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_id',
        'phs_id',
        'qty',
        'unit_price',
        'nettotal',
        'status',
    ];

    public function add($data)
    {
        return $this->create($data);
    }

    public function getPhsById()
    {
        return $this->hasOne(ProductStockHasProducts::class, 'id', 'phs_id')->with('getJobByProductIdforInvoice');
    }

}
