<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_code',
        'location_id',
        'date',
        'po_no',
        'billing_to',
        'billing_address',
        'subtotal',
        'discount',
        'vat',
        'nettotal',
        'remark',
        'status',
    ];

    public function add($data)
    {
        // (new PurchaseOrderController)->createActivity(['view' => 'Purchase Order Added', 'activity' => 'Added']);
        return $this->create($data);
    }

    public function getInvoiceCount()
    {
        return $this::count();
    }

    public function getProductQty($id, $location_id)
    {
        return (new ProductStockHasProducts)->getProductStocksForInvoice($id, $location_id)->selectRaw("SUM(qty) as totqty")->first();
    }

    public function getLocation()
    {
        return $this->hasOne(location::class,'id','location_id');
    }

    public function getInvoiceHasProducts()
    {
        return $this->hasMany(invoice_has_product::class,'invoice_id','id')->with('getPhsById');
    }

    public function getInvoiceRequestsById($id)
    {
        return $this::where('id',$id)->with('getLocation')->with('getInvoiceHasProducts');
    }

    public function getAll()
    {
        return $this::orderBy('id','DESC')->get();
    }

    public function getInvoiceById($id)
    {
        return $this::where('id',$id)->with('getInvoiceHasProducts');
    }

}
