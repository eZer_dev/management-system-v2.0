<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ItemStockExport implements FromArray, WithHeadings
{
    protected $data;
    protected $filter;
    protected $headers;
    public function __construct($data, $filters)
    {
        $this->data = $data;
        $this->filters = $filters;
    }

    function array(): array
    {
        $index = 1;
        foreach ($this->data as $key => $record) {
            $newRec = [$index, $record[0]['item_code'], $record[0]['item_part_code'], $record[0]['item_name'], $record[1]['totqty'],$record[3], ($record[1]['totqty']
                < 5) ? 'YES' : 'NO'];

            if ($this->filters['checked'] == 1) {
                array_splice($newRec, 4, 0, [$record[2]]);
            }

            $records[] = $newRec;

            $index++;
        }

        return $records;
    }

    public function headings(): array
    {
        $this->headers = [
            '#',
            'Item Code',
            'Part Code',
            'Item Name',
            'Quantity',
            'Unit Price',
            'Low Stock',
        ];

        if ($this->filters['checked'] == 1) {
            array_splice($this->headers, 4, 0, ['Bin Location']);
        }

        return $this->headers;
    }
}
