<?php

namespace App\Http\Controllers;

use App\Models\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('dashboard.dashboard');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function routeSuggetions(Request $request)
    {
        $data = array();

        foreach ((new Route)->suggetions($request->all()) as $item) {
            $data[] = [
                'id' => $item->route,
                'name' => $item->name,
            ];
        }

        return response()->json($data, 200);
    }

}
