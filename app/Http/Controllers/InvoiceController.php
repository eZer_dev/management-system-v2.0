<?php

namespace App\Http\Controllers;

use App\Models\bin_location;
use App\Models\invoice;
use App\Models\invoice_has_product;
use App\Models\location;
use App\Models\Product;
use App\Models\ProductStockHasProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class InvoiceController extends Controller
{

    public function index()
    {
        $this->forgetSessions();
        $location = (new location)->getActiveAll();
        $invoiceCode = 'INV/' . date('smy') . '/' . str_pad((new invoice)->getInvoiceCount() + 1, 3, '0', STR_PAD_LEFT);

        return view('dashboard.invoice', compact('location', 'invoiceCode'));

    }

    public function loadProducts(Request $request)
    {
        $data = array();

        foreach ((new Product)->suggetions_to_invoice($request->all()) as $product) {
            $data[] = [
                'id' => $product->id,
                'name' => '(' . $product->code . ')' . ' ' . $product->name . ' - ' . $product['vehicles']->brand . ' ' . $product['vehicles']->model,
            ];
        }
        return response()->json($data, 200);
    }

    public function getProductQtyCount(Request $request)
    {

        if ((new invoice)->getProductQty($request->product_id, $request->location_id)['totqty'] != null) {
            return (new invoice)->getProductQty($request->product_id, $request->location_id)['totqty'];
        } else {
            return 0;
        }

    }

    public function forgetSessions()
    {
        Session::forget('data');
    }

    public function sessionRecords()
    {
        $sessionData = Session::get('data', []);
        return $sessionData;
    }

    public function productAddSession(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'location_id' => 'required',
            'product_id' => 'required',
            'unit_price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|min:0|max:8',
            'qty' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|min:0|max:8',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        // return (new invoice)->getProductQty($request->product_id, $request->location_id);
        // return (new ProductStockHasProducts)->getProductStocksForInvoice($request->product_id, $request->location_id)->get();

        if ((new invoice)->getProductQty($request->product_id, $request->location_id)['totqty'] == null) {
            return 'RESP1';
        }

        if ($request->qty > (new invoice)->getProductQty($request->product_id, $request->location_id)['totqty']) {
            return 'RESP2';
        } else {

            // Session::forget('data');
            // return;

            $isNew = true;
            $data = $this->sessionRecords();

            foreach ($data as $index => $v) {
                if ($data[$index][0] == $request->product_id) {
                    $data[$index][1][0] = $request->product_id;
                    $data[$index][1][1] = $request->unit_price;
                    $data[$index][1][2] = $request->qty;

                    $product_stocks = (new ProductStockHasProducts)->getProductStocksForInvoice($request->product_id, $request->location_id)->get();
                    $invoiceQty = $request->qty;

                    while ($invoiceQty != 0) {
                        foreach ($product_stocks as $key => $value) {
                            if ($value['status'] == 1) {
                                if ($value['qty'] >= $invoiceQty) {
                                    $value['qty'] = $value['qty'] - $invoiceQty;

                                    if ($value['qty'] == 0) {
                                        $value['status'] = 2;
                                    }

                                    $datadummy[] = [
                                        'id' => $value['id'],
                                        'productstock' => $value['productstock'],
                                        'product' => $value['product'],
                                        'bin' => $value['bin'],
                                        'qty' => intval($invoiceQty),
                                        'status' => $value['status'],
                                    ];

                                    $data[$index][2] = $datadummy;
                                    $invoiceQty = 0;

                                    break;

                                } else {
                                    $value['status'] = 2;

                                    $datadummy[] = [
                                        'id' => $value['id'],
                                        'productstock' => $value['productstock'],
                                        'product' => $value['product'],
                                        'bin' => $value['bin'],
                                        'qty' => intval($value['qty']),
                                        'status' => 2,
                                    ];

                                    $invoiceQty = $invoiceQty - $value['qty'];
                                    $value['qty'] = 0;

                                }
                            }
                        }
                    }
                    $isNew = false;
                    break;
                }
            }

            if ($isNew) {

                $product_stocks = (new ProductStockHasProducts)->getProductStocksForInvoice($request->product_id, $request->location_id)->get();
                $invoiceQty = $request->qty;

                while ($invoiceQty != 0) {
                    foreach ($product_stocks as $key => $value) {
                        if ($value['status'] == 1) {
                            if ($value['qty'] >= $invoiceQty) {
                                $value['qty'] = $value['qty'] - $invoiceQty;

                                if ($value['qty'] == 0) {
                                    $value['status'] = 2;
                                }

                                $datadummy[] = [
                                    'id' => $value['id'],
                                    'productstock' => $value['productstock'],
                                    'product' => $value['product'],
                                    'bin' => $value['bin'],
                                    'qty' => intval($invoiceQty),
                                    'status' => $value['status'],
                                ];

                                $data[] = [$request->product_id, [$request->product_id, $request->unit_price, $request->qty], $datadummy];

                                $invoiceQty = 0;

                                break;

                            } else {

                                $value['status'] = 2;

                                $datadummy[] = [
                                    'id' => $value['id'],
                                    'productstock' => $value['productstock'],
                                    'product' => $value['product'],
                                    'bin' => $value['bin'],
                                    'qty' => intval($value['qty']),
                                    'status' => $value['status'],
                                ];

                                $invoiceQty = $invoiceQty - $value['qty'];
                                $value['qty'] = 0;

                            }
                        }
                    }
                }
            }

            Session::put('data', $data);
            return number_format($this->getInvoiceTotal(), 2, '.', ',');

        }
    }

    public function getInvoiceTotal()
    {
        $data_for_tot = Session::get('data');
        $tot = 0;

        if ($data_for_tot == null) {
            return 0;
        } else {
            foreach ($data_for_tot as $key => $val) {
                $tot += $val[1][1] * $val[1][2];
            }
            return $tot;
        }
    }

    public function getInvoiceNettotal(Request $request)
    {

        $dis = $request->discount;
        $vat = $request->vat;

        return number_format($this->getInvoiceTotal() * ((100 - $dis) / 100) * ((100 + $vat) / 100), 2, '.', ',');

    }

    public function getInvoiceNettotal_db(Request $request)
    {
        $dis = $request->dis;
        $vat = $request->vat;

        return $this->getInvoiceTotal() * ((100 - $dis) / 100) * ((100 + $vat) / 100);
    }

    public function invoiceTableView()
    {
        $records = $this->sessionRecords();
        $tableData = [];
        foreach ($records as $index => $val) {
            $bin = array();
            $job = array();
            $tableBinData = '';
            $tableJobData = '';

            foreach ($records[$index][2] as $key => $value) {
                $bin[] = [bin_location::find($value['bin'])->bin_location_name];
                $job[] = (new ProductStockHasProducts)->getJobByProductId($value['id'])->get();
            }

            foreach ($bin as $key => $single_bin) {
                $tableBinData = $tableBinData .
                    '<small
                        class="badge w-100 bg-yellow-transparent-5 text-black-transparent-5 px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                        <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                        ' . $single_bin[0] . '
                    </small>
                    <br>';
            }

            foreach ($job as $key => $single_job) {
                $tableJobData = $tableJobData .
                    '<small
                    class="badge w-100 bg-teal-transparent-4 text-black-transparent-5 px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                    <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                    ' . $single_job[0]['pstock']['hasjob']['code'] . '
                </small>
                <br>';
            }

            $tableData[] = [
                $index + 1,
                Product::find($records[$index][1][0])->code,
                env('CURRENCY') . ' ' . number_format($records[$index][1][1], 2, '.', ','),
                $records[$index][1][2],
                env('CURRENCY') . ' ' . number_format($records[$index][1][1] * $records[$index][1][2], 2, '.', ','),
                $tableBinData,
                $tableJobData,
                '<div class="input-group flex-nowrap">
                    <div class="m-1">
                    <button class="btn btn-default btn-sm" onclick="invoice_removeItem_from_list(' . $index . ')">
                        Remove
                    </button>
                    </div>
                </div>',
            ];
        }
        return $tableData;
    }

    public function removeFromSession(Request $request)
    {
        $index = $request->index;
        $records = $this->sessionRecords();

        if (count($records) > 0) {
            array_splice($records, $index, 1);
            Session::put('data', $records);
            return ['subtotal' => number_format($this->getInvoiceTotal(), 2, '.', ',')];
        } else {
            return 2;
        }
    }

    public function invoiceSave(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'bt' => 'required',
            'ba' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $records = $this->sessionRecords();
        if ($records == null) {
            return 1;
        } else {

            $discount = $request->dis;
            $vat = $request->vat;

            if ($discount == '') {
                $discount = 0;
            }

            if ($vat == '') {
                $vat = 0;
            }

            $data = [
                'location_id' => $request->location,
                'invoice_code' => 'INV/' . date('smy') . '/' . str_pad((new invoice)->getInvoiceCount() + 1, 3, '0', STR_PAD_LEFT),
                'date' => $request->date,
                'po_no' => $request->po,
                'billing_to' => $request->bt,
                'billing_address' => $request->ba,
                'subtotal' => $this->getInvoiceTotal(),
                'discount' => $discount,
                'vat' => $vat,
                'nettotal' => $this->getInvoiceNettotal_db($request),
                'remark' => $request->remark,
                'status' => 1,
            ];

            $invoice = (new invoice)->add($data);

            foreach ($records as $key => $val) {

                foreach ($val[2] as $pkey => $productval) {

                    $phs = ProductStockHasProducts::find($productval['id']);
                    $phs->qty = $phs->qty - $productval['qty'];
                    $phs->status = $productval['status'];
                    $phs->save();

                    $product_stocks_data = [
                        'invoice_id' => $invoice->id,
                        'phs_id' => $productval['id'],
                        'qty' => $productval['qty'],
                        'unit_price' => $val[1][1],
                        'nettotal' => $productval['qty'] * $val[1][1],
                        'status' => 1,
                    ];

                    (new invoice_has_product)->add($product_stocks_data);

                }

            }

            Session::forget('data');
            return ['INV/' . date('smy') . '/' . str_pad((new invoice)->getInvoiceCount() + 1, 3, '0', STR_PAD_LEFT), $invoice->id];

        }

    }

    public function printInvoice(Request $request)
    {
        $data = (new invoice)->getInvoiceRequestsById($request->invoice_id)->get();

        $invoicedata =
            [
            'invoice_code' => $data[0]['invoice_code'],
            'invoice_date' => $data[0]['date'],
            'invoice_po_no' => $data[0]['po_no'],
            'invoice_bt' => $data[0]['billing_to'],
            'invoice_ba' => $data[0]['billing_address'],
            'invoice_subtotal' => $data[0]['subtotal'],
            'invoice_discount' => $data[0]['discount'],
            'invoice_vat' => $data[0]['vat'],
            'nettotal' => $data[0]['nettotal'],
            'invoice_remark' => $data[0]['remark'],
            'invoice_location' => $data[0]['getlocation']['location_name'],
        ];

        $product = array();

        foreach ($data[0]['getinvoicehasproducts'] as $key => $value) {

            // return $value['getphsbyid']['productdata']['name'];

            if (count($product) == 0) {
                $product[] = [$value['getphsbyid']['productdata']['id'], $value['getphsbyid']['productdata']['code'], $value['unit_price'], $value['qty'], $value['getphsbyid']['productdata']['name']];
            } else {
                $isNew = true;
                foreach ($product as $pkey => $val) {
                    if ($value['getphsbyid']['productdata']['id'] == $val[0]) {
                        $product[$pkey][3] = $product[$pkey][3] + $value['qty'];
                        $isNew = false;
                        break;
                    }
                }

                if ($isNew) {
                    $product[] = [$value['getphsbyid']['productdata']['id'], $value['getphsbyid']['productdata']['code'], $value['unit_price'], $value['qty'], $value['getphsbyid']['productdata']['name']];
                }

            }

        }

        $printabledata = ['invoicedetails' => $invoicedata, 'products' => $product];

        if ($printabledata) {
            return view('reports.invoiceReport')->with('data', $printabledata);
            // return $printabledata;
        } else {
            return 2;
        }

    }

    public function invoicedList()
    {
        $invoices = (new invoice)->getAll();
        return view('dashboard.invoicedlist', compact('invoices'));
    }

    public function viewSavedInvoice(Request $request)
    {

        Session::forget('invoice_view_id');

        $data = (new invoice)->getInvoiceRequestsById($request->invoice_id)->get();

        if ($data) {
            $invoicedata =
                [
                'invoice_code' => $data[0]['invoice_code'],
                'invoice_date' => $data[0]['date'],
                'invoice_po_no' => $data[0]['po_no'],
                'invoice_bt' => $data[0]['billing_to'],
                'invoice_ba' => $data[0]['billing_address'],
                'invoice_subtotal' => env('CURRENCY') . ' ' . number_format($data[0]['subtotal'], 2, '.', ','),
                'invoice_discount' => $data[0]['discount'],
                'invoice_vat' => $data[0]['vat'],
                'nettotal' => env('CURRENCY') . ' ' . number_format($data[0]['nettotal'], 2, '.', ','),
                'invoice_remark' => $data[0]['remark'],
                'invoice_location' => $data[0]['getlocation']['location_name'],
            ];

            Session::put('invoice_view_id', $request->invoice_id);

            return $invoicedata;
        } else {
            return 1;
        }

    }

    public function viewSavedInvoiceProductList(Request $request)
    {


        $tableData = [];

        if ($request->session()->has('invoice_view_id')) {

            $invoice_id = $request->session()->get('invoice_view_id');;

            $data = (new invoice)->getInvoiceById($invoice_id)->get();

            if ($data) {

                // return $data[0]['getinvoicehasproducts'];

                foreach ($data[0]['getinvoicehasproducts'] as $index => $value) {
                    $bin = array();
                    $job = array();
                    $tableBinData = '';
                    $tableJobData = '';

                    // return $value;

                    $bin[] = [bin_location::find($value['getphsbyid']['bin'])->bin_location_name];
                    $job[] = (new ProductStockHasProducts)->getJobByProductId($value['phs_id'])->get();

                    foreach ($bin as $key => $single_bin) {
                        $tableBinData = $tableBinData .
                            '<small
                        class="badge w-100 bg-yellow-transparent-5 text-black-transparent-5 px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                        <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                        ' . $single_bin[0] . '
                    </small>
                    <br>';
                    }

                    foreach ($job as $key => $single_job) {
                        $tableJobData = $tableJobData .
                            '<small
                    class="badge w-100 bg-teal-transparent-4 text-black-transparent-5 px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                    <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                    ' . $single_job[0]['pstock']['hasjob']['code'] . '
                </small>
                <br>';
                    }

                    $tableData[] = [
                        $index + 1,
                        Product::find($value['getphsbyid']['product'])->code,
                        Product::find($value['getphsbyid']['product'])->name,
                        env('CURRENCY') . ' ' . number_format($value['unit_price'], 2, '.', ','),
                        $value['qty'],
                        env('CURRENCY') . ' ' . number_format($value['unit_price'] * $value['qty'], 2, '.', ','),
                        $tableBinData,
                        $tableJobData,

                    ];

                }

            }

        }

        return $tableData;

    }

}
