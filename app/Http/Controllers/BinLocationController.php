<?php

namespace App\Http\Controllers;

use App\Models\bin_location;
use App\Models\item;
use App\Models\item_category;
use App\Models\location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

use function PHPUnit\Framework\isEmpty;

class BinLocationController extends Controller
{

    public function index()
    {
        $bin_location = (new bin_location)->getAll();
        $location = (new location)->getActiveAll();

        return view('dashboard.bin', compact('location', 'bin_location'));
    }

    public function getLocationBinLocationSuggetions($lid)
    {
        $data = [];
        foreach ((new bin_location)->getLocationBins($lid) as $item) {
            $data[] = [
                'id' => $item->id,
                'name' => $item->bin_location_name,
            ];
        }

        return response()->json($data, 200);
    }

    public function getLocationBinLocationSuggetionsByProduct($lid, $pid)
    {
        $data = [];
        foreach ((new bin_location)->getLocationBinsByProduct($lid, $pid) as $item) {
            $item_tab = (new item)->where('id', $item->item_id)->first();
            $item_cat = (new item_category)->where('id', $item_tab->item_category_id)->first();
            if ($item_cat->id == 24) {

                $sessionData = Session::get('jobdata');
                if (!empty($sessionData)) {
                    // return $sessionData;
                    foreach ($sessionData as $key => $value) {
                        if ($value[0] != $item->id) {
                            $data[] = [
                                'id' => $item->id,
                                'name' => $item->bin_location_name,
                            ];
                        }
                    }

                } else {
                    $data[] = [
                        'id' => $item->id,
                        'name' => $item->bin_location_name,
                    ];
                }
            }
        }

        return response()->json($data, 200);
    }

    public function loadItem(Request $request)
    {
        $data = array();

        foreach ((new item)->suggetions($request->all()) as $item) {
            $data[] = [
                'id' => $item->id,
                'name' => '(' . $item->item_part_code . ')' . ' ' . $item->item_name,
            ];
        }

        return response()->json($data, 200);
    }

    public function savebin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'location_id' => 'required',
            'bin_name' => 'required',
            'item_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        (new bin_location)->add([
            'location_id' => $request->location_id,
            'item_id' => $request->item_id,
            'bin_location_name' => $request->bin_name,
            'status' => 1,
        ]);

        return 1;

    }

    public function changeStatus(Request $request)
    {

        (new bin_location)->edit('id', $request->id, [
            'status' => $request->status,
        ]);

        return 1;

    }

    public function printAllBins()
    {
        $data = (new bin_location)->getAll();

        if ($data) {
            return view('reports.bins')->with('data', $data);
            // return $data;
        } else {
            return 2;
        }

    }

}
