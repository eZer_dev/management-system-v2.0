<?php

namespace App\Http\Controllers;

use App\Models\item;
use App\Models\itemGeneralIssues;
use App\Models\itemGeneralIssuesItems;
use App\Models\itemGeneralIssuesRecords;
use App\Models\location;
use App\Models\measure_unit;
use App\Models\StockHasItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ItemGeneralIssuesController extends Controller
{
    public function index()
    {

        Session::forget('data');
        Session::forget('session_data');

        $location = (new location())->getActiveAll();
        return view('dashboard.itemGeneralIssues', compact('location'));
    }

    public function getItemSuggetions(Request $request)
    {
        $data = array();

        foreach ((new item)->suggetions($request->all()) as $item) {
            $data[] = [
                'id' => $item->id,
                'name' => $item->item_name . ' ' . '(' . $item->item_part_code . ')',
            ];
        }

        return response()->json($data, 200);
    }

    public function sessionRecords()
    {
        return Session::get('data', []);
    }

    public function sessionSaveItems(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'item_id' => 'required',
            'qty' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|min:0|max:8',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $isNew = true;
        $data = $this->sessionRecords();

        if (Session::has('data')) {
            foreach ($data as $index => $item) {
                if ($item[0] == $request->item_id) {

                    $data[$index][1] = $request->qty;
                    $data[$index][2] = $request->remark;
                    $isNew = false;
                    break;
                }
            }
        }

        if ($isNew) {
            $data[] = [$request->item_id, $request->qty, $request->remark];
        }

        Session::put('data', $data);

        return Session::get('data');

    }

    public function tableView()
    {

        $records = $this->sessionRecords();
        $tableData = [];

        foreach ($records as $key => $item) {

            $item_obj = item::find($item[0]);

            $tableData[] =
                [
                $item_obj->item_name, $item_obj->item_code, $item[1] , '
                    <div class="input-group flex-nowrap">
                        <div class="m-1">
                            <button class="btn btn-round btn-default btn-sm"
                                onclick="removeGeneralIssueItem_session(' . $key . ')">Remove
                            </button>
                        </div>
                    </div>',
            ];
        }

        return $tableData;
    }

    public function removeSessionData(Request $request)
    {

        $index = $request->index;
        $records = $this->sessionRecords();

        if (count($records) > 0) {
            array_splice($records, $index, 1);
            Session::put('data', $records);
            return 1;
        } else {
            return 2;
        }

    }

    public function save(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'location_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $records = $this->sessionRecords();

        if (count($records) == 0) {
            return 1;
        }

        $itemGeneralIssues = (new itemGeneralIssues)->add(
            [
                'generalIssue_code' => 'IGIR/' . str_pad((new itemGeneralIssues)->getItemCount() + 1, 4, '0', STR_PAD_LEFT),
                'location_id' => $request->location_id,
                'manual_id' => $request->manual_id,
                'remark' => $request->gi_remark,
                'status' => 3,
            ]
        );

        foreach ($records as $item) {

            (new itemGeneralIssuesItems)->add(
                [
                    'itemGeneralIssue_id' => $itemGeneralIssues->id,
                    'item_id' => $item[0],
                    'qty' => $item[1],
                    'in_qty' => 0,
                    'remark' => $item[2],
                    'status' => 3,
                ]
            );
        }

        Session::forget('data');

        return 2;

    }

    public function allIssueRequests_tableView()
    {

        $generalIssues = (new itemGeneralIssues)->getAll();

        $tableData = [];
        $index = 1;

        foreach ($generalIssues as $generalIssue) {

            switch ($generalIssue->status) {
                case 1:
                    $statusText = 'Approved';
                    $statusColor1 = 'success';
                    $statusColor2 = 'green';
                    break;
                case 2:
                    $statusText = 'Refused';
                    $statusColor1 = 'danger';
                    $statusColor2 = 'red';
                    break;
                case 3:
                    $statusText = 'Pending';
                    $statusColor1 = 'warning';
                    $statusColor2 = 'yellow';
                    break;
                case 4:
                    $statusText = 'Processing';
                    $statusColor1 = 'purple';
                    $statusColor2 = 'indigo';
                    break;
                case 5:
                    $statusText = 'Issue Completed';
                    $statusColor1 = 'primary';
                    $statusColor2 = 'blue';
                    break;
                default:
                    $statusText = '-';
                    $statusColor1 = 'default';
                    $statusColor2 = 'white';
                    break;
            }

            $status = '<span class="badge my-1 bg-'
                . $statusColor2 .
                '-100 text-'
                . $statusColor1 .
                ' px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                    <i class="fa fa-circle text-' . $statusColor1 . ' fs-9px fa-fw me-5px"></i>'
                . $statusText .
                '</span>';

            $actions = '<div class="input-group flex-nowrap">
                            <div class="m-1">

                                ' . ((true) ? '<a onclick="print_GeneralIssue(' . $generalIssue->id . ')" class="btn btn-default btn-sm">
                                    <i class="fa fa-print" aria-hidden="true"></i> Print </a>' : '') . '

                                ' . (($generalIssue->status == 3) ? '<button onclick="approval_GeneralIssue(' . $generalIssue->id . ')" class="btn btn-teal btn-sm">
                                    <i class="fa fa-check"></i></button> <button onclick="refuse_GeneralIssue(' . $generalIssue->id . ')" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i></button>' : '') . '

                                ' . ((($generalIssue->status == 1) || ($generalIssue->status == 4)) ? '<button onclick="itemIssue4_GeneralIssue(' . $generalIssue->id . ')" class="btn btn-primary btn-sm">
                                    <i class="fa fa-pencil" aria-hidden="true"></i> Issue Items </button>' : '') . '

                            </div>
                        </div>';

            $tableData[] = [

                $index,
                $generalIssue->created_at->format('d M Y'),
                $generalIssue->generalIssue_code,
                location::find($generalIssue->location_id)->location_name,
                $generalIssue->manual_id,
                (($generalIssue->remark != null) ? $generalIssue->remark : ' - '),
                $status,
                $actions,

            ];

            $index++;

        }

        return $tableData;

    }

    public function approval(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        if ($request->status == 1) {
            itemGeneralIssues::find($request->id)->update(array('status' => 1));
            return 1;
        }

        if ($request->status == 0) {
            itemGeneralIssues::find($request->id)->update(array('status' => 2));
            return 2;
        }

    }

    public function allIssueRequestsView_session(Request $request)
    {
        Session::put('session_data', (new itemGeneralIssuesItems)->where(['itemGeneralIssue_id' => $request->id])->get());
    }

    public function allIssueRequestsView_tableView()
    {
        $tableData = [];
        $records = null;

        if (Session::has('session_data')) {
            $records = Session::get('session_data');

            foreach ($records as $key => $item) {

                $item_obj = item::find($item->item_id);
                
                $pending_val = ($item->qty - $item->in_qty).' '.measure_unit::find($item_obj->measure_unit_id)->symbol;
                
                // $symbol = (new measure_unit)->where('id', $$item_obj->id)

                $tableData[] =
                    [
                    $item_obj->item_name,
                    $item_obj->item_code,
                    // $item->qty,
                    $item->qty . ' ' . measure_unit::find($item_obj->measure_unit_id)->symbol,
                    // $item->in_qty,
                    $item->in_qty . ' ' . measure_unit::find($item_obj->measure_unit_id)->symbol,
                    '<input id="item_general_issue_remark_view_itemqty' . $key . '" class="form-control form-control-sm" type="number" placeholder=" Pending Qty :  ' . $pending_val . '" />',
                    '<div class="input-group flex-nowrap">

                        <div class="m-1">
                            <button class="btn btn-round btn-yellow btn-sm"
                                onclick="qty_reseteGeneralIssueItem_view(item_general_issue_remark_view_itemqty' . $key . ')"> <i class="fa fa-refresh" aria-hidden="true"></i>
                            </button>
                        </div>

                        <div class="m-1">
                            <button class="btn btn-round btn-success btn-sm"
                                onclick="qty_updateGeneralIssueItem_db(item_general_issue_remark_view_itemqty' . $key . ',' . $item->id . ')"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update
                            </button>
                        </div>
                    </div>',
                ];

            }

        }

        return $tableData;

    }

    public function generalIssue_report(Request $request)
    {

        $data = (new itemGeneralIssues)->printReport($request->id);

        // return $data;

        if ($data) {
            return view('reports.itemGeneralIssue')->with('data', $data);
        } else {
            return 1;
        }

    }

    public function generalIssue_qtyUpdate(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'value' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/|min:0|max:8',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $obj = itemGeneralIssuesItems::find($request->id);

        if (($obj->qty - $obj->in_qty) >= $request->value && $request->value > 0) {

            $generalItem = (new itemGeneralIssues)->where('id', $obj->itemGeneralIssue_id)->first();

            $isProcess = false;

            if ((new itemGeneralIssues)->getItemQty($obj->item_id, $generalItem->location_id)['totqty'] != null) {
                if ((new itemGeneralIssues)->getItemQty($obj->item_id, $generalItem->location_id)['totqty'] >= $request->value) {
                    $isProcess = true;
                } else {
                    return response()->json([
                        'code' => 2,
                        'type' => 'error',
                        'des' => ['Invalid stock Qty', 'Invalid stock quantity, stock has only ' . (new itemGeneralIssues)->getItemQty($obj->item_id, $generalItem->location_id)['totqty'] . ' of qty', 'OK'],
                        'refresh_status' => 0,
                        'feild_reset_status' => 1,
                    ]);
                }
            } else {
                return response()->json([
                    'code' => 3,
                    'type' => 'error',
                    'des' => 'Inserted item not in selected location',
                    'refresh_status' => 0,
                    'feild_reset_status' => 1,
                ]);
            }

            if ($isProcess) {

                $item_stock = (new StockHasItems)->getItemsForGeneralIssues($obj->item_id, $generalItem->location_id)->get();
                $issueQty = $request->value;

                while ($issueQty != 0) {

                    foreach ($item_stock as $key => $value) {

                        if ($value['status'] == 1) {
                            if ($value['qty'] >= $issueQty) {

                                $qty = $issueQty;

                                $value['qty'] = $value['qty'] - $issueQty;

                                if ($value['qty'] == 0) {
                                    $value['status'] = 2;
                                }

                                (new StockHasItems)->where('id', $value['id'])->update(
                                    [
                                        'qty' => $value['qty'],
                                        'status' => $value['status'],
                                    ]
                                );

                                (new itemGeneralIssuesRecords)->add(
                                    [
                                        'generalIssue_id' => $generalItem->id,
                                        'generalIssueItem_id' => $request->id,
                                        'stock_id' => $value['stock_id'],
                                        'item_id' => $value['item_id'],
                                        'bin_location_id' => $value['bin_location_id'],
                                        'qty' => $issueQty,
                                        'status' => 1,
                                    ],
                                );

                                $issueQty = 0;
                                break;

                                // $datadummy[] = [
                                //     'stock_id' => $value['stock_id'],
                                //     'item_id' => $value['item_id'],
                                //     'bin_location_id' => $value['bin_location_id'],
                                //     'qty'=> $value['qty'],
                                //     'unit_price' => $value['unit_price'],
                                //     'status' => $value['status'],
                                // ];

                            } else {

                                $qty = $value['qty'];

                                $value['status'] = 2;
                                $issueQty = $issueQty - $value['qty'];
                                $value['qty'] = 0;

                                (new StockHasItems)->where('id', $value['id'])->update(
                                    [
                                        'qty' => $value['qty'],
                                        'status' => $value['status'],
                                    ]
                                );

                                (new itemGeneralIssuesRecords)->add(
                                    [
                                        'generalIssue_id' => $generalItem->id,
                                        'generalIssueItem_id' => $request->id,
                                        'stock_id' => $value['stock_id'],
                                        'item_id' => $value['item_id'],
                                        'bin_location_id' => $value['bin_location_id'],
                                        'qty' => $qty,
                                        'status' => 1,
                                    ],
                                );

                            }

                        }

                    }

                }

                $obj->update(array('in_qty' => $obj->in_qty + $request->value));

                $checkstatus = (new itemGeneralIssues)->getGeneralItems($obj->itemGeneralIssue_id);
                $isUpdateStatus = true;

                foreach ($checkstatus['generalitems'] as $key => $item) {
                    if ($item->qty != $item->in_qty) {
                        itemGeneralIssues::find($checkstatus->id)->update(array('status' => 4));
                        $isUpdateStatus = false;
                        break;
                    }
                }

                if ($isUpdateStatus) {
                    itemGeneralIssues::find($checkstatus->id)->update(array('status' => 5));
                }

            }

            return response()->json([
                'code' => 4,
                'type' => 'success',
                'des' => 'Item issuing successfull',
                'refresh_status' => 1,
                'feild_reset_status' => 1,
            ]);

        } else {
            return response()->json([
                'code' => 1,
                'type' => 'error',
                'des' => 'In valid qty inserted',
                'refresh_status' => 0,
                'feild_reset_status' => 1,
            ]);
        }
    }
}
