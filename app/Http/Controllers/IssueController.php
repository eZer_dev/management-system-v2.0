<?php

namespace App\Http\Controllers;

use App\Models\bin_location;
use App\Models\issuing;
use App\Models\issuingitemreturn;
use App\Models\issuingitems;
use App\Models\Job;
use App\Models\material_request;
use App\Models\MRProductsHasItem;
use App\Models\StockHasItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class IssueController extends Controller
{
    protected $sessionKey1 = 'issuedata';

    public function index()
    {
        return view('dashboard.pages.issuing');
    }

    public function next($mrid)
    {
        if ($mrid != null) {
            $this->loadToSession($this->loadMaterialItems($mrid));
        }

        return $this->genarateNextId($mrid);
    }

    public function genarateNextId($mrid)
    {
        return env('ISSUEPREFIX', '') . str_pad((new issuing)->getNextId(), 3, "0", STR_PAD_LEFT);
    }

    public function loadMaterialItems($mrid)
    {
        return (new MRProductsHasItem)->getMaterialRequestItemsByMRID($mrid);
    }

    public function loadToSession($data)
    {
        $this->sessionData(null, true);

        $records = [];

        foreach ($data as $key => $value) {
            $records[] = [$value->id, $value->job_has_product_id, $value->material_request_id, $value->totqty, $value['items'], $value->available, $value->totqty];
        }

        $this->sessionData($records);
    }

    public function sessionData($data = null, $reset = false)
    {
        if ($reset == true) {
            Session::put($this->sessionKey1, []);
        }

        if ($data != null) {
            Session::put($this->sessionKey1, $data);
        }

        return Session::get($this->sessionKey1, []);
    }

    public function tableView()
    {
        $data = $this->sessionData();

        foreach ($data as $key => $value) {
            $data[$key] = [$key + 1, $value[4]['item_code'], $value[4]['item_part_code'], $value[4]['item_name'], $value[3], (($value[3] > -1) ? '<i onclick="editMaterialHasItemRecord(' . $value[4]['id'] . ',' . $value[6] . ')" class="fa fa-edit text-warning" title="Edit Quantity"></i>' : '')];
        }

        return $data;
    }

    public function editSession($mriid, $qty)
    {
        $data = $this->sessionData();
        if ($qty > -1) {
            if ((new MRProductsHasItem)->getMaterialRequestItemByMRIID($mriid)->totqty >= $qty) {
                foreach ($data as $key => $value) {
                    if ($value[4]['id'] == $mriid) {
                        $data[$key][3] = $qty;
                        break;
                    }
                }
                $this->sessionData($data);
            } else {
                return 3;
            }
        } else {
            return 2;
        }
    }

    public function enroll(Request $request)
    {
        $data = $this->sessionData();
        if (count($data) > 0) {
            $request->validate([
                'formkey' => 'required|integer|exists:material_requests,id',
            ]);

            $bins = $this->getValidBins($request->formkey);

            foreach ($data as $keycheck1 => $valuecheck1) {

                if ($valuecheck1[3] > 0) {
                    $que = StockHasItems::where('qty', '>', 0)->where('item_id', $valuecheck1[4]->id)->whereIn('bin_location_id', $bins)->selectRaw("item_id,SUM(qty) as totqty")
                        ->groupBy('item_id')->with('item')->get();

                    if (count($que) == 0) {
                        return 'Invalid Stock Available For ' . $valuecheck1[4]['item_code'];
                    }

                    foreach ($que as $key21 => $value21) {
                        if ($value21->totqty < $valuecheck1[3]) {
                            return 'Invalid Stock Available For ' . $value21['item']->item_code;
                        }
                    }
                }

            }

            $issueData = [
                'code' => $this->next(null),
                'mrequest' => $request->formkey,
                'user' => Auth::user()->id,
                'status' => 1,
            ];

            if ($request->has('remark')) {
                $issueData['remark'] = $request->remark;
            }

            $issue = (new issuing)->add($issueData, ['view' => Session::get('view', 'non'), 'activity' => 'Create New Job']);

            foreach ($data as $key => $value) {

                if ($value[3] > 0) {
                    $qty1 = $value[3];
                    $qty = $value[3];

                    foreach (MRProductsHasItem::where('material_request_id', $request->formkey)->where('item_id', $value[4]->id)->where('available', '>', 0)->get() as $mrikey => $mrivalue) {
                        if ($mrivalue->available >= $qty1) {
                            MRProductsHasItem::where('id', $mrivalue->id)->update(['available' => ($mrivalue->available - $qty1)]);
                            break;
                        } else {
                            MRProductsHasItem::where('id', $mrivalue->id)->update(['available' => 0]);
                            $qty1 = $qty1 - $mrivalue->available;
                            if ($qty1 == 0) {
                                break;
                            }
                        }
                    }

                    foreach (StockHasItems::where('qty', '>', 0)->where('item_id', $value[4]->id)->whereIn('bin_location_id', $bins)->get() as $key1 => $value1) {

                        if ($value1->qty >= $qty) {
                            StockHasItems::where('id', $value1->id)->update([
                                'qty' => ($value1->qty - $qty),
                            ]);

                            (new issuingitems)->add([
                                'issuing' => $issue->id,
                                'shi' => $value1->id,
                                'qty' => $qty,
                            ]);

                            break;
                        } else {
                            StockHasItems::where('id', $value1->id)->update([
                                'qty' => 0,
                            ]);

                            (new issuingitems)->add([
                                'issuing' => $issue->id,
                                'shi' => $value1->id,
                                'qty' => $value1->qty,
                            ]);

                            $qty = $qty - $value1->qty;

                            if ($qty == 0) {
                                break;
                            }
                        }
                    }
                }
            }

            $isAllEmpty = true;

            foreach ($this->loadMaterialItems($request->formkey) as $key3 => $value3) {
                if ($value3->totqty > 0) {
                    $isAllEmpty = false;
                    break;
                }
            }

            if ($isAllEmpty == true) {
                material_request::where('id', $request->formkey)->update(['status' => 4]);
            }

            return 1;
        } else {
            return 2;
        }
    }

    public function getValidBins($mrid)
    {
        $ids = [];

        foreach (bin_location::where('location_id', Job::where('id', material_request::where('id', $mrid)->first()->job_id)->first()->location)->get() as $key => $value) {
            $ids[] = $value->id;
        }

        return $ids;
    }

    public function dataTableView($sdate, $edate)
    {
        $data = [];

        foreach ($this->getDataRecords($sdate, $edate) as $key => $value) {

            $data[] = [$key + 1, $value->code, $value['materialdata']['mr_code'], $value['userdata']['fname'] . ' ' . (($value['userdata']['lname']) ? $value['userdata']['lname'] : ''), $value->created_at->format('Y/m/d'), '<span class="badge bg-' . (($value->status == 1) ? 'green' : 'red') . '-100 text-' . (($value->status == 1) ? 'success' : 'danger') . ' px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center"><i class="fa fa-circle text-' . (($value->status == 1) ? 'teal' : 'danger') . ' fs-9px fa-fw me-5px"></i>' . (($value->status == 1) ? 'Issued' : 'Not Issued') . '</span>', '<div class="input-group flex-nowrap">
            <div class="m-1">
                <button class="btn btn-secondary btn-sm" onclick="openIssueViewModal(' . $value->id . ')">
                    View / Return
                </button>
                <button class="btn btn-default btn-sm" onclick="printIssue(' . $value->id . ')">
                    Print
                </button>
            </div>
            </div>'];
        }

        return $data;
    }

    public function dataTablePrintView($sdate, $edate)
    {
        return view('reports.issueListReport')->with('data', ['records' => $this->getDataRecords($sdate, $edate), 'filters' => [$sdate, $edate]]);
    }

    public function getDataRecords($sdate, $edate)
    {
        return (new issuing)->getData($sdate, $edate);
    }

    public function getIssueData($iid)
    {
        $data = (new issuing)->getIssue($iid);
        $data['date'] = $data->created_at->format('Y/m/d');
        return $data;
    }

    public function returnIssue(Request $request)
    {

        $ihiid = $request->ihiid;
        $qty = $request->qty;

        $issueitem = issuingitems::where('id', $ihiid)->first();

        if ($issueitem && $qty>0) {

            $availableQty = $issueitem->qty;

            foreach (issuingitemreturn::where('issuing', $issueitem->issuing)->where('shi', $issueitem->shi)->get() as $key => $value) {
                $availableQty = $availableQty - $value->qty;
            }

            if ($availableQty >= $qty) {
                $save = [
                    'user' => Auth::user()->id,
                    'issuing' => $issueitem->issuing,
                    'shi' => $issueitem->shi,
                    'qty' => $qty,
                ];

                $save['remark'] = $request->remark;

                issuingitemreturn::create($save);

                $stockHasItem=StockHasItems::where('id',$issueitem->shi)->first();

                StockHasItems::where('id',$issueitem->shi)->update(['qty'=>($stockHasItem->qty+$qty)]);

                return [1,$this->getIssueData($issueitem->issuing)];
            } else {
                return [3,'Return quantity exceed'];
            }
        } else {
            return [2];
        }
    }

    public function print($iid)
    {
        return view('reports.issuePrint')->with('data',(new issuing)->getIssue($iid));
    }
}
