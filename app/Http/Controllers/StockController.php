<?php

namespace App\Http\Controllers;

use App\Exports\ItemStockExport;
use App\Models\bin_location;
use App\Models\GRN;
use App\Models\GRNHasItems;
use App\Models\issuingitems;
use App\Models\item;
use App\Models\location;
use App\Models\StockHasItems;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class StockController extends Controller
{
    public function index(Request $request)
    {
        return view('dashboard.pages.stock')->with('data', [
            'locations' => (new LocationController)->getLocations(),
        ]);
    }

    public function tableView($itemid, $grnid, $from, $to, $bin, $locationid, $isChecked)
    {
        $tableData = [];

        foreach ($this->getRecordsFromFilters($itemid, $grnid, $from, $to, $bin, $locationid, $isChecked) as $key => $record) {
            $item = (new item)->getItemById($record['item_id']);
            $tableData[] = [$key + 1, $item->item_code, $item->item_part_code, (($isChecked == 1) ? ((new bin_location)->getBinLocationById($record->bin_location_id)->bin_location_name) : 'Multiple Bins'), $item->item_name, $record->totqty . ' ' . $item['munit']['symbol'], ($record->totqty < env('LOWSTOCK')) ? '<span class="badge bg-red-100 text-danger px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center"><i class="fa fa-circle text-danger fs-9px fa-fw me-5px"></i>Low Stock</span>' : '-', '<span class="badge bg-' . (($record->totqty > 0) ? 'green' : 'red') . '-100 text-' . (($record->totqty > 0) ? 'success' : 'danger') . ' px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center"><i class="fa fa-circle text-' . (($record->totqty > 0) ? 'teal' : 'danger') . ' fs-9px fa-fw me-5px"></i>' . (($record->totqty > 0) ? 'Available' : 'Unavailable') . '</span>'];
        }

        return $tableData;
    }

    public function getRecordsFromFilters($itemid, $grnid, $from, $to, $bin, $locationid, $isChecked)
    {

        $filters = [
            'startgrndate' => null,
            'endgrndate' => null,
            'binlocation' => null,
            'locationid' => null,
            'grnid' => null,
            'item_id' => null,
        ];

        if ($itemid != '0') {
            $filters['item_id'] = $itemid;
        }
        if ($grnid != '0') {
            $filters['grnid'] = $grnid;
        }
        if ($from != '0') {
            $filters['startgrndate'] = $from;
        }
        if ($to != '0') {
            $filters['endgrndate'] = $to;
        }
        if ($bin != '0') {
            $filters['binlocation'] = $bin;
        }
        if ($locationid != '0') {
            $filters['locationid'] = $locationid;
        }

        return (new GRN)->getStock($filters, (($isChecked == 1) ? true : false));
    }

    public function printReport($itemid, $grnid, $from, $to, $bin, $locationid, $isChecked)
    {
        $filters = [
            'grn' => (new GRN)->getGrn($grnid),
            'item' => (new item)->getItemById($itemid),
            'from' => $from,
            'to' => $to,
            'location' => (new location)->getLocationbyId($locationid),
            'bin' => (new bin_location)->getBinLocationById($bin),
            'checked' => $isChecked,
        ];

        $records = [];

        $recs = $this->getRecordsFromFilters($itemid, $grnid, $from, $to, $bin, $locationid, $isChecked);

        if (count($recs) > 0) {
            foreach ($recs as $key => $record) {
                $records[] = [(new item)->getItemById($record['item_id']), $record, (($isChecked == 1) ? ((new bin_location)->getBinLocationById($record->bin_location_id)->bin_location_name) : 'Multiple Bins')];
            }

            return view('reports.stockReport')->with('data', ['filters' => $filters, 'records' => $records]);
        } else {
            return 2;
        }
    }

public function exportReport($itemid, $grnid, $from, $to, $bin, $locationid, $isChecked)
    {
        $filters = [
            'grn' => (new GRN)->getGrn($grnid),
            'item' => (new item)->getItemById($itemid),
            'from' => $from,
            'to' => $to,
            'location' => (new location)->getLocationbyId($locationid),
            'bin' => (new bin_location)->getBinLocationById($bin),
            'checked' => $isChecked,
        ];

        $records = [];

        $recs = $this->getRecordsFromFilters($itemid, $grnid, $from, $to, $bin, $locationid, $isChecked);

        if (count($recs) > 0) {

            foreach ($recs as $key => $record) {
                $records[] = [(new item)->getItemById($record['item_id']), $record, (($isChecked == 1) ? ((new bin_location)->getBinLocationById($record->bin_location_id)->bin_location_name) : 'Multiple Bins'),$record->unit_price];
            }

            return Excel::download(new ItemStockExport($records, $filters),Session::get('view', 'non') .'.xlsx');

        } else {
            return redirect()->back()->with(['code' => 1, 'color' => 'warning', 'msg' => 'No Records To Export.']);
        }

    }

    public function transaction($item, $from, $to)
    {
        if ((new item)->getItemById($item) != null && $this->validateDate($from) && $this->validateDate($to)) {
            $from1 = date($from);
            $to1 = date($to);
            $grnItems = GRNHasItems::where('item_id', $item)->whereBetween('created_at', [$from1, $to1])->with('grn')->get();
            $stockhasitems = StockHasItems::where('item_id', $item)->with('issuingitemslist')->with('returningitemslist')->get();

            if (count($grnItems) > 0) {
                $records=[];
                foreach ($this->displayDates($from,$to) as $key => $value) {
                    foreach ($grnItems as $keygrn => $valuegrn) {
                        if(strtotime($value) == strtotime($valuegrn['grn']['created_at'])){
                            $records[$value]['grns'][]=$valuegrn;
                        }
                    }
                    foreach ($stockhasitems as $shikey => $shivalue) {
                        foreach ($shivalue['issuingitemslist'] as $keyissue => $valueissue) {
                            if(strtotime($valueissue['created_at']->format('Y-m-d'))<strtotime($to) && strtotime($valueissue['created_at']->format('Y-m-d'))>strtotime($from)){
                                if(strtotime($value) == strtotime($valueissue['created_at']->format('Y-m-d'))){
                                    $records[$value]['issues'][]=$valueissue;
                                }
                            }
                        }
                        foreach ($shivalue['returningitemslist'] as $keyissue1 => $valueissue1) {
                            if(strtotime($valueissue1['created_at']->format('Y-m-d'))<strtotime($to) && strtotime($valueissue1['created_at']->format('Y-m-d'))>strtotime($from)){
                                if(strtotime($value) == strtotime($valueissue1['created_at']->format('Y-m-d'))){
                                    $records[$value]['returns'][]=$valueissue1;
                                }
                            }
                        }
                    }
                }

                if(count($records)>0){
                    return view('reports.itemtransactions')->with('data',$records);
                } else {
                    return 2;
                }
            } else {
                return 2;
            }
        } else {
            return 3;
        }
    }

    public function displayDates($date1, $date2, $format = 'Y-m-d')
    {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while ($current <= $date2) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
    }

    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
}
