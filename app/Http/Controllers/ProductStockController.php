<?php

namespace App\Http\Controllers;

use App\Models\bin_location;
use App\Models\Job;
use App\Models\location;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class ProductStockController extends Controller
{
    public function index(Request $request)
    {
        $data = [
            'locations' => (new LocationController)->getLocations(),
        ];
        return view('dashboard.pages.productstock')->with('data', $data);
    }

    public function getJobs(Request $request)
    {
        $data = array();

        foreach ((new Job)->suggetions($request->all()) as $item) {
            $data[] = [
                'id' => $item->id,
                'name' => $item->code,
            ];
        }

        return response()->json($data, 200);
    }

    public function tableView($job, $vehicle, $product, $from, $to, $bin, $location, $wise)
    {
        $tableData = [];

        foreach ($this->getRecordsFromFilters($job, $vehicle, $product, $from, $to, $bin, $location, $wise) as $key => $record) {
            $tableData[] = [$key + 1, $record['productdata']['code'], $record['productdata']['name'], (($wise == 1) ? ((new bin_location)->getBinLocationById($record->bin)->bin_location_name) : 'Multiple Bins'), $record->totqty, ($record->totqty < env('LOWSTOCK')) ? '<span class="badge bg-red-100 text-danger px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center"><i class="fa fa-circle text-danger fs-9px fa-fw me-5px"></i>Low Stock</span>' : '-', '<span class="badge bg-' . (($record->totqty > 0) ? 'green' : 'red') . '-100 text-' . (($record->totqty > 0) ? 'success' : 'danger') . ' px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center"><i class="fa fa-circle text-' . (($record->totqty > 0) ? 'teal' : 'danger') . ' fs-9px fa-fw me-5px"></i>' . (($record->totqty > 0) ? 'Available' : 'Unavailable') . '</span>'];
        }

        return $tableData;
    }

    public function getRecordsFromFilters($job, $vehicle, $product, $from, $to, $bin, $location, $wise)
    {

        $filters = [
            'job' => null,
            'vehicle' => null,
            'product' => null,
            'from' => null,
            'to' => null,
            'bin' => null,
            'location' => null,
            'wise' => null,
        ];

        if ($job != '0') {
            $filters['job'] = $job;
        }
        if ($vehicle != '0') {
            $filters['vehicle'] = $vehicle;
        }
        if ($product != '0') {
            $filters['product'] = $product;
        }
        if ($from != '0') {
            $filters['from'] = $from;
        }
        if ($to != '0') {
            $filters['to'] = $to;
        }
        if ($bin != '0') {
            $filters['bin'] = $bin;
        }
        if ($location != '0') {
            $filters['location'] = $location;
        }

        return (new ProductStock)->getData($filters, (($wise == 1) ? true : false));
    }

    public function printReport($job, $vehicle, $product, $from, $to, $bin, $location, $wise)
    {
        $filters = [
            'job' => (new Job)->getRecord($job),
            'vehicle' => (new Vehicle)->getData($vehicle),
            'product' => (new Product)->getData($product),
            'from' => $from,
            'to' => $to,
            'location' => (new location)->getLocationbyId($location),
            'bin' => (new bin_location)->getBinLocationById($bin),
            'checked' => $wise,
        ];

        $recs = [];

        foreach ($this->getRecordsFromFilters($job, $vehicle, $product, $from, $to, $bin, $location, $wise) as $key => $record) {
            $recs[] = [$key + 1, $record['productdata']['code'], $record['productdata']['name'], (($wise == 1) ? ((new bin_location)->getBinLocationById($record->bin)->bin_location_name) : 'Multiple Bins'), $record->totqty];
        }

        if (count($recs) > 0) {
            return view('reports.productStockReport')->with('data', ['filters' => $filters, 'records' =>  $recs]);
        } else {
            return 2;
        }
    }
}
