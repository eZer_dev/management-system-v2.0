<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('location_id');
            $table->string('invoice_code');
            $table->datetime('date');
            $table->string('po_no')->nullable();
            $table->string('billing_to')->nullable();
            $table->longtext('billing_address')->nullable();
            $table->double('subtotal');
            $table->double('discount')->nullable();
            $table->double('vat')->nullable();
            $table->double('nettotal');
            $table->longText('remark')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
