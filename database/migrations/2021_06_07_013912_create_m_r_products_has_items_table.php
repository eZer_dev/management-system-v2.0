<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMRProductsHasItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_r_products_has_items', function (Blueprint $table) {
            $table->id();
            $table->integer('job_has_product_id');
            $table->integer('material_request_id');
            $table->integer('item_id');
            $table->double('qty');
            $table->double('available')->default(0);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_r_products_has_items');
    }
}
